package gone.server.gameengine;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;


/**
 * Created by matthias on 11.08.16.
 */
public class PreGameEngineTest {
    private RedcodeParser rcParser;
    private List<InstructionTemplate> list1;
    private List<InstructionTemplate> list2;
    private Core core;
    private Warrior warrior1;
    private Warrior warrior2;
    private PreGameEngine pge;

    @Before
    public void setUp() {
        rcParser = new RedcodeParser();
        list1 = rcParser.parse("MOV 0 1 \n MOV #0 1 \n");
        list2 = rcParser.parse("ADD 1 0 \n ADD #1 0 \n");
        core = new Core(5);
        WarriorProcess w1P = new WarriorProcess();
        WarriorProcess w2P = new WarriorProcess();
        warrior1 = new Warrior("warrior1", w1P, list1);
        warrior2 = new Warrior("warrior2", w2P, list2);
        pge = new PreGameEngine(2, 2, core);
        pge.setInitialWarriorList(warrior1, warrior2);
        pge.copyWarriorsToCore();
    }

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testSetPlayground() {
        thrown.expect(IllegalArgumentException.class);
        pge.setPlayground(new Core());
    }

    @Test
    public void testSetInitialWarriorList_sameName() {
        thrown.expect(IllegalArgumentException.class);
        warrior1 = new Warrior("abc", new WarriorProcess(), list1);
        warrior2 = new Warrior("abc", new WarriorProcess(), list2);
        pge.setInitialWarriorList(warrior1, warrior2);
    }

    @Test
    public void testSetMaxPlayerCount() {
        thrown.expect(IllegalArgumentException.class);
        pge.setMaxPlayerCount(0);
    }

    @Test
    public void testSetMaxStartInstructionsCount_1() {
        thrown.expect(IllegalArgumentException.class);
        pge.setMaxStartInstructionsCount(0);
    }


    @Test
    public void testSetMaxStartInstructionsCount_2() {
        thrown.expect(IllegalArgumentException.class);
        pge.setPlayground(new Core(5));
        pge.setMaxStartInstructionsCount(9000);
    }

    @Test
    public void testSetInitialWarriorList() {
        List<Warrior> initW = pge.getInitialWarriorList();

        // start player at pos 0, second player at pos 1 at list
        assertTrue("start player", initW.get(0).isStartPlayer());
        assertFalse("second player", initW.get(1).isStartPlayer());
        assertTrue("player count", initW.size() == PreGameEngine.getMaxPlayerCount());

        int movPlayerIndex = 0;
        int addPlayerIndex = 0;

        for (int i = 0; i < PreGameEngine.getMaxPlayerCount(); i++) {
            if (initW.get(i).getRcProgram().get(0).getOperationCode().equals("ADD")
                    && initW.get(i).getRcProgram().get(1).getOperationCode().equals("ADD")) {
                addPlayerIndex = i;
            }
            if (initW.get(i).getRcProgram().get(0).getOperationCode().equals("MOV")
                    && initW.get(i).getRcProgram().get(1).getOperationCode().equals("MOV")) {
                movPlayerIndex = i;
            }
        }

        // warrior 1
        List<InstructionTemplate> w1List = initW.get(movPlayerIndex).getRcProgram();
        testInstrTemplate(w1List.get(0), "MOV", FieldPrefix.Direct, 0,
                FieldPrefix.Direct, 1);
        testInstrTemplate(w1List.get(1), "MOV", FieldPrefix.Immediate, 0,
                FieldPrefix.Direct, 1);
        // warrior 2
        List<InstructionTemplate> w2List = initW.get(addPlayerIndex).getRcProgram();
        testInstrTemplate(w2List.get(0), "ADD", FieldPrefix.Direct, 1,
                FieldPrefix.Direct, 0);
        testInstrTemplate(w2List.get(1), "ADD", FieldPrefix.Immediate, 1,
                FieldPrefix.Direct, 0);
    }

    @Test
    public void testCopyWarriorsToCore() {
        int warriorsInstrCount = 0;
        List<Warrior> wList = pge.getInitialWarriorList();

        for (Warrior temp : wList) {
            warriorsInstrCount += temp.getInstructionsCount();
        }

        // check the core elements <-> warrior elements
        Core tempCore = core;
        int datCountW = tempCore.getSize() - warriorsInstrCount;
        int coreInstrCount = 0;

        for (Warrior temp : wList) {
            for (int k = 0; k < temp.getInstructionsCount(); k++) {
                CoreElement tempCe =
                        pge.convertInstrTemplateToCoreElement(temp.getRcProgram().get(k));

                // iterate over core, search the CE that fits tempCe
                for (int i = 0; i < tempCore.getSize(); i++) {
                    CoreElement coreCe = tempCore.getCoreElement(i);
                    // test if same values
                    if (tempCe.areEqualFields(coreCe)) {
                        coreInstrCount++;
                        tempCore.getCoreList().remove(i);
                    }
                }
            }
        }

        // check dat count
        tempCore = core;
        int datCountC = 0;
        for (int i = 0; i < tempCore.getSize(); i++) {
            CoreElement temp = tempCore.getCoreElement(i);
            if (temp instanceof CoreElementDat
                    && temp.getAField().getFieldPrefix() == FieldPrefix.Direct
                    && temp.getBField().getFieldPrefix() == FieldPrefix.Direct) {
                datCountC++;
            }
        }

        assertEquals("Instruction Count Warriorlist <-> Core", warriorsInstrCount, coreInstrCount);
        assertEquals(datCountC, datCountW);
    }

    // help function for testSetInitialWarriorList
    private void testInstrTemplate(InstructionTemplate toTest, String keyWord,
                                   FieldPrefix aMode, int aAddress, FieldPrefix bMode, int bAddress) {
        assertEquals("OpCode", keyWord, toTest.getOperationCode());
        assertEquals("A Mode", aMode, toTest.getAField().getFieldPrefix());
        assertEquals("A Address", aAddress, toTest.getAField().getAddress());
        assertEquals("B Mode", bMode, toTest.getBField().getFieldPrefix());
        assertEquals("B Address", bAddress, toTest.getBField().getAddress());
    }

    @Test
    public void testSetInitWarriorList_maxStartInstructionsCount() {
        // we expect a IllegalArgumentException
        thrown.expect(IllegalArgumentException.class);

        int maxStartInstructionsCount = 2;
        // instrCount (3) > maxStartInstructionsCount (2)
        list1 = rcParser.parse("MOV 0 1 \n MOV #0 1 \n MOV #0 1 \n");
        core = new Core(5);
        WarriorProcess w1P = new WarriorProcess();
        warrior1 = new Warrior("warrior1", w1P, list1);
        pge = new PreGameEngine(maxStartInstructionsCount, 2, core);
        pge.setInitialWarriorList(warrior1, warrior2);
    }

    @Test
    public void testSetInitWarriorList_maxPlayerCountCoreSize() {
        // we expect a IllegalArgumentException
        thrown.expect(IllegalArgumentException.class);

        int maxStartInstructionsCount = 10;
        int maxPlayerCount = 2;
        // warrior1: instrCount > core.size / maxPlayerCount
        list1 = rcParser.parse("MOV 0 1 \n MOV #0 1 \n MOV #0 1 \n");
        list2 = rcParser.parse("ADD 0 1 \n ADD #0 1 \n");
        core = new Core(4);

        WarriorProcess w1P = new WarriorProcess();
        WarriorProcess w2P = new WarriorProcess();
        warrior1 = new Warrior("warrior1", w1P, list1);
        warrior2 = new Warrior("warrior2", w2P, list2);

        pge = new PreGameEngine(maxStartInstructionsCount, maxPlayerCount, core);
        pge.setInitialWarriorList(warrior1, warrior2);
    }

    // help function to test CoreElements
    private void checkCoreElement(Core playground, int index, Class<?> instruction,
                                  FieldPrefix fieldPrefixA, int addressA,
                                  FieldPrefix fieldPrefixB, int addressB) {
        if (playground.getCoreElement(index) instanceof CoreElementInstruction) {
            assertTrue("is instruction", playground.getCoreElement(index)
                    instanceof CoreElementInstruction);
            assertThat("instruction type",
                    ((CoreElementInstruction) playground.getCoreElement(index)).getInstruction(),
                    instanceOf(instruction));
        } else {
            assertTrue("is dat", playground.getCoreElement(index) instanceof CoreElementDat);
        }
        assertEquals("a fieldPrefix", fieldPrefixA, playground.getCoreElement(index).getAField().getFieldPrefix());
        assertEquals("a address", addressA, playground.getCoreElement(index).getAField().getAddress());
        assertEquals("b fieldPrefix", fieldPrefixB, playground.getCoreElement(index).getBField().getFieldPrefix());
        assertEquals("b address", addressB, playground.getCoreElement(index).getBField().getAddress());
    }

    @Test
    public void testSetInitWarriorList_onePlayer() {
        PreGameEngine preGameEngine = new PreGameEngine(8, 1, new Core(8));
        List<InstructionTemplate> dwarf = new RedcodeParser().parse(
                "ADD #4 3 \n MOV 2 @2 \n JMP -2 \n DAT #0 #0");
        Warrior dwarf1 = new Warrior("dwarf", new WarriorProcess(), dwarf);
        preGameEngine.setInitialWarriorList(dwarf1);

        assertTrue("1 warrior in list", preGameEngine.getInitialWarriorList().size() == 1);

        preGameEngine.copyWarriorsToCore();
        Core playground = preGameEngine.getPlayground();
        int dwarfStart = preGameEngine.getStartPositionByWarrior(dwarf1);

        assertEquals("start pos", preGameEngine.getStartPositionByWarrior(dwarf1),
                preGameEngine.getInitialWarriorList().get(0).getStartAddressAtCore());

        checkCoreElement(playground, dwarfStart, AddInstruction.class,
                FieldPrefix.Immediate, 4, FieldPrefix.Direct, 3);
        checkCoreElement(playground, dwarfStart + 1, MoveInstruction.class,
                FieldPrefix.Direct, 2, FieldPrefix.Indirect, 2);
        checkCoreElement(playground, dwarfStart + 2, JumpInstruction.class,
                FieldPrefix.Direct, -2, FieldPrefix.Direct, 0);
        checkCoreElement(playground, dwarfStart + 3, null,
                FieldPrefix.Immediate, 0, FieldPrefix.Immediate, 0);

        // rest: DAT 0 0
        for (int i = 0; i < 4; i++) {
            checkCoreElement(playground, dwarfStart + 4 + i, null,
                    FieldPrefix.Direct, 0, FieldPrefix.Direct, 0);
        }
    }

    @Test
    public void convertInstrTemplateToCoreElement() {
        // normal case 1
        List<InstructionTemplate> toTest1 = rcParser.parse("MOV 0 1");
        CoreElement mov = pge.convertInstrTemplateToCoreElement(toTest1.get(0));
        assertTrue("Instruction", mov instanceof CoreElementInstruction);
        assertTrue("MOV", ((CoreElementInstruction) mov).getInstruction()
                instanceof MoveInstruction);
        assertEquals("aMode", FieldPrefix.Direct, mov.getAField().getFieldPrefix());
        assertEquals("aAddress", 0, mov.getAField().getAddress());
        assertEquals("bMode", FieldPrefix.Direct, mov.getBField().getFieldPrefix());
        assertEquals("bAddress", 1, mov.getBField().getAddress());

        // normal case 2 (DAT)
        List<InstructionTemplate> toTest2 = rcParser.parse("DAT #0 #1");
        CoreElement dat = pge.convertInstrTemplateToCoreElement(toTest2.get(0));
        assertTrue("DAT", dat instanceof CoreElementDat);
        assertEquals("aMode", FieldPrefix.Immediate, dat.getAField().getFieldPrefix());
        assertEquals("aAddress", 0, dat.getAField().getAddress());
        assertEquals("bMode", FieldPrefix.Immediate, dat.getBField().getFieldPrefix());
        assertEquals("bAddress", 1, dat.getBField().getAddress());

        // error case 1: we expect a IllegalArgumentException (MVO)
        thrown.expect(IllegalArgumentException.class);
        List<InstructionTemplate> mvo = rcParser.parse("MVO 0 1");
        pge.convertInstrTemplateToCoreElement(mvo.get(0));
    }

    @Test
    public void testGetStartPositionByWarrior() {
        assertEquals("start Pos by Warrior",
                pge.getStartPositionByWarrior(warrior1), warrior1.getStartAddressAtCore());
    }

    @Test
    public void testInitiateCore() {
        int size = 10;
        Core toTest1 = pge.initiateCore(size);
        Core toTest2 = new Core(size);

        assertEquals("Core Size", toTest1.getSize(), toTest2.getSize());

        for (int i = 0; i < size; i++) {
            CoreElement coreElement1 = toTest1.getCoreElement(i);
            CoreElement coreElement2 = toTest2.getCoreElement(i);
            assertTrue("CoreElements same parameters", coreElement1.areEqualFields(coreElement2));
        }
    }

    @Test
    public void testIdentifyStartPlayer() {
        for (int i = 0 ; i < 100; i++) {
            int rand01 = pge.identifyStartPlayer();
            assertTrue("random 0 or 1", (rand01 == 0 || rand01 == 1));
        }
    }

    @Test
    public void testStartPlayerFirstInstructionAtCore() {
        // normal case 1
        int toCheck = pge.startPlayerFirstInstructionAtCore();
        assertTrue("toCheck index inside Core Area",
                (toCheck >= 0 && toCheck < core.getSize()));

        // border case
        Core miniCore = new Core(1);
        pge.setPlayground(miniCore);
        int toCheck2 = pge.startPlayerFirstInstructionAtCore();
        assertTrue("toCheck index inside Core Area",
                (toCheck2 >= 0 && toCheck2 < miniCore.getSize()));

        // error case - we expect a IllegalArgumentException
        thrown.expect(IllegalArgumentException.class);
        Core emptyCore = new Core(0);
        pge.setPlayground(emptyCore);
        pge.startPlayerFirstInstructionAtCore();
    }

    @Test
    public void testSecondPlayerFirstInstructionAtCore_1() {
        list1 = rcParser.parse("MOV 0 1 \n MOV #0 1 \n");
        list2 = rcParser.parse("ADD 1 0 \n ADD #1 0 \n");
        core = new Core(10);
        warrior1 = new Warrior("warrior1", new WarriorProcess(), list1);
        warrior2 = new Warrior("warrior2", new WarriorProcess(), list2);
        pge = new PreGameEngine(2, 2, core);
        pge.setInitialWarriorList(warrior1, warrior2);
        pge.copyWarriorsToCore();

        assertEquals("pge list size", 2, pge.getInitialWarriorList().size());
        assertTrue("1st player is start player",
                pge.getInitialWarriorList().get(0).isStartPlayer());
        assertFalse("2nd player is NOT start player",
                pge.getInitialWarriorList().get(1).isStartPlayer());

        int startPlayerIndex = 0;
        int secondPlayerIndex = 0;

        for (int i = 0; i < pge.getInitialWarriorList().size(); i++) {
            if (pge.getInitialWarriorList().get(i).isStartPlayer()) {
                startPlayerIndex = i;
            } else {
                secondPlayerIndex = i;
            }
        }

        int startPlayerFirstInstruction =
                pge.getInitialWarriorList().get(startPlayerIndex).getStartAddressAtCore();
        int secondPlayerFirstInstruction =
                pge.getInitialWarriorList().get(secondPlayerIndex).getStartAddressAtCore();

        // normal cases
        assertTrue("index not equal", startPlayerIndex != secondPlayerIndex);
        assertTrue("startAddress 1st/2nd not equal",
                startPlayerFirstInstruction != secondPlayerFirstInstruction);


        // test that area doesn't overlap
        Warrior wTest1 = pge.getInitialWarriorList().get(startPlayerIndex);
        Warrior wTest2 = pge.getInitialWarriorList().get(secondPlayerIndex);
        int warrior1Start = wTest1.getStartAddressAtCore();
        int warrior2Start = wTest2.getStartAddressAtCore();

        for (int i = 0; i < wTest1.getInstructionsCount(); i++) {
            int w1check = (warrior1Start + i) % core.getSize();
            for (int k = 0; k < wTest2.getInstructionsCount(); k++) {
                assertFalse("not overlapping",
                        w1check == (warrior2Start + k) % core.getSize());
            }
        }

        // error cases - we expect a IllegalArgumentException
        thrown.expect(IllegalArgumentException.class);
        pge.setPlayground(new Core());
        pge.secondPlayerFirstInstructionAtCore(0, 1);
    }

    @Test
    public void testSecondPlayerFirstInstructionAtCore_2() {
        thrown.expect(IllegalArgumentException.class);
        Core core = new Core(5);
        pge.setPlayground(core);
        pge.secondPlayerFirstInstructionAtCore(1, 1);
    }

    @Test
    public void testSecondPlayerFirstInstructionAtCore_3() {
        thrown.expect(IllegalArgumentException.class);
        pge.secondPlayerFirstInstructionAtCore(0, 0);
    }

    @Test
    public void testSecondPlayerFirstInstructionAtCore_4() {
        thrown.expect(IllegalArgumentException.class);
        // core too small
        Core errorCore = new Core(3); // warrior 1 and 2 have each 2 instructions
        pge.setPlayground(errorCore);
        pge.secondPlayerFirstInstructionAtCore(0, 1);
    }
}
