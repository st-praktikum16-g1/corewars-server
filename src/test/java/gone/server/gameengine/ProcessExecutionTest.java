package gone.server.gameengine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

import gone.server.parser.RedcodeParser;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;


/**
 * Created by winfr on 27.06.2016.
 */
public class ProcessExecutionTest {
    /**
     * Tests to warriors with one process each.
     */
    @Test
    public void testWarriorRunQueueRun_twoProcesses() {
        ProcessStub process1 = new ProcessStub();
        ProcessStub process2 = new ProcessStub();

        List<InstructionTemplate> instructionList = new RedcodeParser().parse("JMP 0");

        Warrior warrior1 = new Warrior("w1", process1, instructionList);
        Warrior warrior2 = new Warrior("w2", process2, instructionList);

        warrior1.setStartPlayer(true);
        warrior2.setStartPlayer(false);

        List<Warrior> warriorList = new LinkedList<>();
        warriorList.add(warrior1);
        warriorList.add(warrior2);

        WarriorRunQueue warriorQueue = new WarriorRunQueue(warriorList);

        for (int i = 0; i < 10; i++) {
            warriorQueue.run(new Core());
            assertTrue(process1.hasBeenCalled());
            assertFalse(process2.hasBeenCalled());
            process1.reset();

            warriorQueue.run(new Core());
            assertFalse(process1.hasBeenCalled());
            assertTrue(process2.hasBeenCalled());
            process2.reset();
        }
    }

    /**
     * Tests two warriors with one or more processes each.
     */
    @Test
    public void testWarriorRunQueueRun_multipleProcesses() {
        ProcessStub process1 = new ProcessStub();
        ProcessStub process21 = new ProcessStub();
        ProcessStub process22 = new ProcessStub();

        List<InstructionTemplate> instructionList = new RedcodeParser().parse("JMP 0");

        Warrior warrior1 = new Warrior("w1", process1, instructionList);
        Warrior warrior2 = new Warrior("w2", process21, instructionList);
        warrior2.getWarriorProcessQueue().add(process22);

        warrior1.setStartPlayer(true);
        warrior2.setStartPlayer(false);

        List<Warrior> warriorList = new LinkedList<>();
        warriorList.add(warrior1);
        warriorList.add(warrior2);

        WarriorRunQueue warriorQueue = new WarriorRunQueue(warriorList);

        for (int i = 0; i < 10; i++) {
            warriorQueue.run(new Core());
            assertTrue(process1.hasBeenCalled());
            assertFalse(process21.hasBeenCalled());
            assertFalse(process22.hasBeenCalled());
            process1.reset();

            warriorQueue.run(new Core());
            assertFalse(process1.hasBeenCalled());
            assertTrue(process21.hasBeenCalled());
            assertFalse(process22.hasBeenCalled());
            process21.reset();

            warriorQueue.run(new Core());
            assertTrue(process1.hasBeenCalled());
            assertFalse(process21.hasBeenCalled());
            assertFalse(process22.hasBeenCalled());
            process1.reset();

            warriorQueue.run(new Core());
            assertFalse(process1.hasBeenCalled());
            assertFalse(process21.hasBeenCalled());
            assertTrue(process22.hasBeenCalled());
            process22.reset();
        }
    }
}
