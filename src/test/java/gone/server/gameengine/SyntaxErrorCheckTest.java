package gone.server.gameengine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.IInstruction;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.CompareInstruction;
import gone.server.instructions.DecrementAndJumpIfNotZeroInstruction;
import gone.server.instructions.JumpIfNotZeroInstruction;
import gone.server.instructions.JumpIfZeroInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;
import gone.server.instructions.SkipIfLessThan;
import gone.server.instructions.SplitInstruction;
import gone.server.instructions.SubInstruction;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by matthias on 26.06.16.
 */
public class SyntaxErrorCheckTest {
    /**
     * This method tests ALL INSTRUCTIONS if the used address-mode for each type of
     * instruction is correct. This is needed, so our PreGameEngine doesn't apply
     * InstructionTemplates to the Core, that are syntactic false. Later on the GameEngine
     * can also check the Syntax, so we don't execute syntactic false Instructions.
     */
    private RedcodeParser rcParser;
    private List<InstructionTemplate> templateList;

    @Before
    public void setUp() {
        rcParser = new RedcodeParser();
    }

    @Test
    public void testMov_immediateAddressingCases() {
        templateList = rcParser.parse("MOV 0 1 \n MOV #0 1 \n MOV 0 #1");
        IInstruction moveInstruction = new MoveInstruction();
        // normal case
        assertTrue("MOV A B", moveInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        assertTrue("MOV #A B", moveInstruction.checkIfSyntaxCorrect(templateList.get(1)));
        // error case
        assertFalse("MOV A #B", moveInstruction.checkIfSyntaxCorrect(templateList.get(2)));
    }

    @Test
    public void testAdd_immediateAddressingCases() {
        templateList = rcParser.parse("ADD @0 1 \n ADD #0 1 \n ADD 0 #1");
        IInstruction addInstruction = new AddInstruction();
        // normal case
        assertTrue("ADD @A B", addInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        assertTrue("ADD #A B", addInstruction.checkIfSyntaxCorrect(templateList.get(1)));
        // error case
        assertFalse("ADD A #B", addInstruction.checkIfSyntaxCorrect(templateList.get(2)));
    }

    @Test
    public void testSub_immediateAddressingCases() {
        templateList = rcParser.parse("SUB <0 1 \n SUB #0 1 \n SUB 0 #1");
        IInstruction subInstruction = new SubInstruction();
        // normal case
        assertTrue("SUB <A B", subInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        assertTrue("SUB #A B", subInstruction.checkIfSyntaxCorrect(templateList.get(1)));
        // error case
        assertFalse("SUB A #B", subInstruction.checkIfSyntaxCorrect(templateList.get(2)));
    }

    @Test
    public void testJmp_immediateAddressingCases() {
        templateList = rcParser.parse("JMP @0 \n JMP #0");
        IInstruction jmpInstruction = new JumpInstruction();
        // normal case
        assertTrue("JMP @A", jmpInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("JMP #A", jmpInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    @Test
    public void testJmz_immediateAddressingCases() {
        templateList = rcParser.parse("JMZ 0 \n JMZ #0");
        IInstruction jmzInstruction = new JumpIfZeroInstruction();
        // normal case
        assertTrue("JMZ A", jmzInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("JMZ #A", jmzInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    @Test
    public void testJmn_immediateAddressingCases() {
        templateList = rcParser.parse("JMN 0 \n JMN #0");
        IInstruction jmnInstruction = new JumpIfNotZeroInstruction();
        // normal case
        assertTrue("JMN A", jmnInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("JMN #A", jmnInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    @Test
    public void testDjn_immediateAddressingCases() {
        templateList = rcParser.parse("DJN 0 \n DJN #0");
        IInstruction djnInstruction = new DecrementAndJumpIfNotZeroInstruction();
        // normal case
        assertTrue("DJN A", djnInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("DJN #A", djnInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    @Test
    public void testCmp_immediateAddressingCases() {
        templateList = rcParser.parse("CMP #0 @1 \n CMP 0 1 \n CMP 0 #1");
        IInstruction cmpInstruction = new CompareInstruction();
        // normal cases
        assertTrue("CMP #A @B", cmpInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        assertTrue("CMP A B", cmpInstruction.checkIfSyntaxCorrect(templateList.get(1)));
        // error case
        assertFalse("CMP A #B", cmpInstruction.checkIfSyntaxCorrect(templateList.get(2)));
    }

    @Test
    public void testSpl_immediateAddressingCases() {
        templateList = rcParser.parse("SPL @0 1 \n SPL #0 1");
        IInstruction splInstruction = new SplitInstruction();
        // normal case
        assertTrue("SPL @A B", splInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("SPL #A B", splInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    @Test
    public void testSlt_immediateAddressingCases() {
        templateList = rcParser.parse("SLT <0 1 \n SLT 0 #1");
        IInstruction sltInstruction = new SkipIfLessThan();
        // normal case
        assertTrue("SLT <A B", sltInstruction.checkIfSyntaxCorrect(templateList.get(0)));
        // error case
        assertFalse("SLT A #B", sltInstruction.checkIfSyntaxCorrect(templateList.get(1)));
    }

    /**
     * Here we should test, if the pseudo-Operation SPACE is accepted or not.
     * We expect that SPACE ist NOT accepted, because in ICWS88 SPACE is not a valid pseudo-op.
     * The Parser will throw a IllegalArgumentException for SPACE. (operation code is invalid)
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDoesSpaceInstructionExist() throws IllegalArgumentException {
        templateList = rcParser.parse("SPACE 0 1");
        // parse() method here throws IllegalArgumentException
    }
}
