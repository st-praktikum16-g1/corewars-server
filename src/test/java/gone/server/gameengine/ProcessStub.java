package gone.server.gameengine;

import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.core.Core;

/**
 * Created by winfr on 27.06.2016.
 */
public class ProcessStub implements IProcess {
    private boolean called;

    ProcessStub() {
        called = false;
    }

    public void reset() {
        called = false;
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        called = true;
    }

    @Override
    public void setInstructionPointer(int instructionPointer) {

    }

    @Override
    public int getInstructionPointer() {
        return 0;
    }

    public boolean hasBeenCalled() {
        return called;
    }
}
