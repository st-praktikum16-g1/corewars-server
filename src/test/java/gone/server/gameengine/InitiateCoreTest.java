package gone.server.gameengine;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.FieldPrefix;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Before;
import org.junit.Test;

import java.util.List;


/**
 * Created by marti on 27.06.2016.
 */
public class InitiateCoreTest {
    private static GameEngine gameEngine;
    private static PreGameEngine preGameEngine;

    private static Core core;
    private static RedcodeParser parser;
    private Warrior warriorA;
    private Warrior warriorB;
    private List<InstructionTemplate> warriorProgramA;
    private List<InstructionTemplate> warriorProgramB;

    private static final int CORESIZE = 800;
    private static final String IMP = "MOV 0 1";
    private static final String DWARF = "ADD #4 3\n"
                                        + "MOV 2 @2\n"
                                        + "JMP -2\n"
                                        + "DAT #0 #0";


    /**
     * this function instantiates the core, the parser and the pre-game engine class
     */
    @Before
    public void createSetup() {
        parser = new RedcodeParser();
        preGameEngine = new PreGameEngine(10, 2, new Core(CORESIZE));
    }

    /**
     * this function tests whether the size of the core parameter is set correctly
     * @throws Exception
     */
    @Test
    public void testSetCoreSize() throws Exception {
        core = new Core(CORESIZE);
        assertEquals("correct core size", CORESIZE, core.getSize());
    }

    /**
     * this test function determines whether all core fields are set to dat 0,0 correctly
     * @throws Exception
     */
    @Test
    public void initiateCore() throws Exception {
        core = preGameEngine.initiateCore(CORESIZE);
        for (int j = 0; j < CORESIZE; j++) {
            assertThat("is dat element", core.getCoreElement(j), instanceOf(CoreElementDat.class));
            assertEquals("a field is 0", 0, core.getCoreElement(j).getAField().getAddress());
            assertEquals("b field is 0", 0, core.getCoreElement(j).getBField().getAddress());
        }
    }

    // help function to test CoreElements
    private void checkCoreElement(Core playground, int index, Class<?> instruction,
                                  FieldPrefix fieldPrefixA, int addressA,
                                  FieldPrefix fieldPrefixB, int addressB) {
        if (playground.getCoreElement(index) instanceof CoreElementInstruction) {
            assertTrue("is instruction", playground.getCoreElement(index)
                    instanceof CoreElementInstruction);
            assertThat("instruction type",
                    ((CoreElementInstruction) playground.getCoreElement(index)).getInstruction(),
                    instanceOf(instruction));
        } else {
            assertTrue("is dat", playground.getCoreElement(index) instanceof CoreElementDat);
        }
        assertEquals("a fieldPrefix",
                fieldPrefixA, playground.getCoreElement(index).getAField().getFieldPrefix());
        assertEquals("a address",
                addressA, playground.getCoreElement(index).getAField().getAddress());
        assertEquals("b fieldPrefix",
                fieldPrefixB, playground.getCoreElement(index).getBField().getFieldPrefix());
        assertEquals("b address",
                addressB, playground.getCoreElement(index).getBField().getAddress());
    }

    /**
     * this function tests if the two warrior programs are loaded correctly to their
     * starting position within the core.
     * @throws Exception
     */
    @Test
    public void testLoadWarrior() throws Exception {
        warriorProgramA = parser.parse(IMP);
        warriorProgramB = parser.parse(DWARF);
        WarriorProcess testWarriorProcess = new WarriorProcess();
        warriorA = new Warrior("Arstan", testWarriorProcess, warriorProgramA);
        warriorB = new Warrior("Belwas", testWarriorProcess, warriorProgramB);

        preGameEngine.setInitialWarriorList(warriorA, warriorB);
        preGameEngine.copyWarriorsToCore();
        int startA = preGameEngine.getStartPositionByWarrior(warriorA);
        int startB = preGameEngine.getStartPositionByWarrior(warriorB);
        Core playground = preGameEngine.getPlayground();

        // arstan
        checkCoreElement(playground, startA, MoveInstruction.class, FieldPrefix.Direct,
                0, FieldPrefix.Direct, 1);

        // belwas
        checkCoreElement(playground, startB, AddInstruction.class, FieldPrefix.Immediate,
                4, FieldPrefix.Direct, 3);
        checkCoreElement(playground, startB + 1, MoveInstruction.class, FieldPrefix.Direct,
                2, FieldPrefix.Indirect, 2);
        checkCoreElement(playground, startB + 2, JumpInstruction.class, FieldPrefix.Direct,
                -2, FieldPrefix.Direct, 0);
        checkCoreElement(playground, startB + 3, null, FieldPrefix.Immediate,
                0, FieldPrefix.Immediate, 0);
    }
}
