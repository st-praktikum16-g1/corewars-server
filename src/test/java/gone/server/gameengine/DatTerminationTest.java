package gone.server.gameengine;

import static org.junit.Assert.assertFalse;

import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.parser.InstructionTemplate;

import gone.server.parser.RedcodeParser;
import org.junit.Test;

import java.util.List;

/**
 * Created by marti on 27.06.2016.
 */
public class DatTerminationTest {

    @Test
    public void testDatTermination() {
        RedcodeParser parser = new RedcodeParser();
        Core core = new Core(1);

        List<InstructionTemplate> warriorProgram = parser.parse("DAT #0 #0");
        core.setCoreElement(0, new CoreElementDat(0, 0));

        WarriorProcess testWarriorProcess = new WarriorProcess();
        Warrior warrior = new Warrior("Warrior", testWarriorProcess, warriorProgram);
        warrior.setStartAddressAtCore(0);

        // TODO: might use copy warrior to core here

        warrior.run(core);
        assertFalse(warrior.getIsAlive());
    }
}
