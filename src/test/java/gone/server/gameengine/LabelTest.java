package gone.server.gameengine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.RcArgument;

import org.junit.Test;


/**
 * Created by Ferdinand on 22.09.16.
 */
public class LabelTest {
    private RcArgument test = new RcArgument();

    @Test
    public void hasReferenceLabelTest() {
        assertFalse(test.hasReferencedLabel());
        test.setReferencedLabel("abc");
        assertTrue(test.hasReferencedLabel());
    }

    @Test
    public void getReferencedLabelTest() {
        test.setReferencedLabel("def");
        assertEquals(test.getReferencedLabel(), "def");
    }
}
