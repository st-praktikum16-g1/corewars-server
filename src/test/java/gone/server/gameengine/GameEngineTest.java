package gone.server.gameengine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.CoreElement;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.LinkedList;
import java.util.List;



/**
 * Created by matthias on 19.08.16.
 */
public class GameEngineTest {
    private Core playCore;
    private GameEngine gameEngineE;
    private RedcodeParser rcParser;
    private Warrior warrior1;
    private Warrior warrior2;
    private List<InstructionTemplate> list1;
    private List<InstructionTemplate> list2;
    private List<Warrior> warriorList;
    private GameInfo gameInfo;

    @Before
    public void setUp() {
        rcParser = new RedcodeParser();
        playCore = new Core(10);

        WarriorProcess w1P = new WarriorProcess();
        WarriorProcess w2P = new WarriorProcess();
        list1 = rcParser.parse("MOV 0 1 \n MOV 0 1 \n");
        list2 = rcParser.parse("MOV 0 1 \n MOV 0 1 \n");
        warrior1 = new Warrior("w1", w1P, list1);
        warrior2 = new Warrior("w2", w2P, list2);
        warriorList = new LinkedList<>();
        warriorList.add(warrior1);
        warriorList.add(warrior2);
    }

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGeConstructorException_maxCycles() {
        thrown.expect(IllegalArgumentException.class);
        gameEngineE = new GameEngine(0, warriorList , playCore);
    }

    @Test
    public void testGeConstructorException_emptyWarriorList() {
        thrown.expect(IllegalArgumentException.class);
        gameEngineE = new GameEngine(100, new LinkedList<>() , playCore);
    }

    @Test
    public void testGeConstructorException_emptyCore() {
        thrown.expect(IllegalArgumentException.class);
        gameEngineE = new GameEngine(100, warriorList , new Core());
    }

    @Test
    public void testSetWarriorRunQueueException_emptyList() {
        thrown.expect(IllegalArgumentException.class);
        gameEngineE = new GameEngine(100, warriorList , new Core(100));
        gameEngineE.setWarriorRunQueue(new LinkedList<>());
    }

    @Test
    public void testAreBothWarriorsAlive() {
        gameEngineE = new GameEngine(100, warriorList , new Core(100));
        assertTrue("all warriors alive", gameEngineE.areAllWarriorsAlive());
        gameEngineE.getWarriorRunQueue().getWarriorRunQueue().remove();
        assertTrue("not all warriors alive", !gameEngineE.areAllWarriorsAlive());
        assertTrue("1 warrior at Queue",
                1 == gameEngineE.getWarriorRunQueue().getWarriorRunQueue().size());
        gameEngineE.getWarriorRunQueue().getWarriorRunQueue().remove();
        assertTrue("0 warrior at Queue",
                0 == gameEngineE.getWarriorRunQueue().getWarriorRunQueue().size());
    }

    @Test
    public void testGameStepBothAlive() {
        list1 = rcParser.parse("JMP 0");
        list2 = rcParser.parse("DAT #0 #1");
        WarriorProcess p1 = new WarriorProcess();
        WarriorProcess p2 = new WarriorProcess();
        warrior1 = new Warrior("w1", p1, list1);
        warrior2 = new Warrior("w2", p2, list2);
        PreGameEngine pge = new PreGameEngine(1, 2, new Core(5));
        pge.setInitialWarriorList(warrior1, warrior2);

        pge.copyWarriorsToCore();
        gameEngineE = new GameEngine(100, pge.getInitialWarriorList(), pge.getPlayground());

        for (int i = 0; i < 10; i++) {

            if (gameEngineE.areAllWarriorsAlive()) {
                Warrior currentW =
                        gameEngineE.getWarriorRunQueue().getWarriorRunQueue().element();
                CoreElement currentCe = gameEngineE.getPlayCore().getCoreElement(
                        currentW.getWarriorProcessQueue().getCurrent().getInstructionPointer());
                gameInfo = gameEngineE.gameStep();

                // 1 (!) DAT warrior gets executed
                if (currentCe instanceof CoreElementDat) {
                    assertFalse("1 warrior died", gameEngineE.areAllWarriorsAlive() );
                    assertFalse("game status", gameInfo.getIsGameRunning());
                }
            }
        }
    }

    @Test
    public void testGameStep_onlyMov() {
        WarriorProcess w1P = new WarriorProcess();
        WarriorProcess w2P = new WarriorProcess();
        list1 = rcParser.parse("MOV 0 1 \n");
        list2 = rcParser.parse("MOV 0 1 \n");
        warrior1 = new Warrior("w1", w1P, list1);
        warrior2 = new Warrior("w2", w2P, list2);

        PreGameEngine pge = new PreGameEngine(2, 2, new Core(5));
        pge.setInitialWarriorList(warrior1, warrior2);
        pge.copyWarriorsToCore();
        gameEngineE = new GameEngine(100, pge.getInitialWarriorList() , pge.getPlayground());

        for (int i = 0; i < 10; i++) {
            gameInfo = gameEngineE.gameStep();  // gameRound++ in gameStep()
            assertTrue("game status", gameInfo.getIsGameRunning());
            assertTrue("game round", i + 1 == gameInfo.getRoundNumber());
        }
    }
}
