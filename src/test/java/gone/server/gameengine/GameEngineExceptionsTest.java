package gone.server.gameengine;

import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

/**
 * Created by Ferdinand on 29.09.16.
 */
public class GameEngineExceptionsTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testNegativeSizeForConstructors() {
        thrown.expect(IllegalArgumentException.class);
        Core playground = new Core(-1);
    }

    @Test
    public void testWrongOpcade_1() {
        thrown.expect(IllegalArgumentException.class);

        PreGameEngine preGameEngine = new PreGameEngine(2, 1, new Core(2));

        preGameEngine.getInstructionTypeFromString("ABC");
    }

    @Test
    //tests exception in PGE setInitialWarriorList() for one warrior
    public void testToLargeWarrior_1() {
        thrown.expect(IllegalArgumentException.class);


        String code = String.join("\n",
                "ADD #2 1",
                "DAT #0 #3",
                "");
        PreGameEngine firstPreGameEngine = new PreGameEngine(1, 1, new Core(10));
        List<InstructionTemplate> list = new RedcodeParser().parse(code);
        Warrior firstWarrior = new Warrior("forExecution", new WarriorProcess(), list);
        firstPreGameEngine.setMaxStartInstructionsCount(1);

        firstPreGameEngine.setInitialWarriorList(firstWarrior);
    }

    @Test
    //tests exception in PGE setInitialWarriorList() for two warriors
    public void testToLargeWarrior_2() {
        thrown.expect(IllegalArgumentException.class);

        String code = String.join("\n",
                "ADD #2 1",
                "DAT #0 #3");
        PreGameEngine firstPreGameEngine = new PreGameEngine(1, 1, new Core(10));
        List<InstructionTemplate> list = new RedcodeParser().parse(code);
        Warrior firstWarrior = new Warrior("firstWarrior", new WarriorProcess(), list);
        Warrior secondWarrior = new Warrior("secondWarrior", new WarriorProcess(), list);
        firstPreGameEngine.setMaxStartInstructionsCount(1);

        firstPreGameEngine.setInitialWarriorList(firstWarrior, secondWarrior);
    }
}
