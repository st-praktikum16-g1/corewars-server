package gone.server.operator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import gone.server.core.Core;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by matthias on 25.08.16.
 */
public class ServerGameLoopTest {
    private ServerGameLoop gameLoop;
    private RedcodeParser rcParser;

    private Warrior warrior1;
    private Warrior warrior2;
    private List<InstructionTemplate> list1;
    private List<InstructionTemplate> list2;

    @Before
    public void setUp() {
        rcParser = new RedcodeParser();
    }

    // TODO: test MAX_CYCLES, Execution: 1p, 2p, draw, not draw …

    @Test
    public void testGameLoop_movDraw1() throws InterruptedException {
        list1 = rcParser.parse("MOV 0 1 \n");
        warrior1 = new Warrior("w1", new WarriorProcess(), list1);
        PreGameEngine pge = new PreGameEngine(2, 1, new Core(5));
        pge.setInitialWarriorList(warrior1);
        pge.copyWarriorsToCore();
        gameLoop = new ServerGameLoop(50, pge.getInitialWarriorList(), pge.getPlayground(), 10);

        gameLoop.gameLoop();
        Thread.sleep(11 * 50);

        GameResult result = gameLoop.getGameResult();
        assertEquals("player count", result.getPlayerCount(), 1);
        assertEquals("round Number", result.getRoundNumber(), 50);
        assertTrue("is draw", result.isDraw());
    }

    @Test
    public void testGameLoop_noDraw1() throws InterruptedException {
        list1 = rcParser.parse("DAT #0 #1 \n");
        warrior1 = new Warrior("w1", new WarriorProcess(), list1);
        PreGameEngine pge = new PreGameEngine(2, 1, new Core(5));
        pge.setInitialWarriorList(warrior1);
        pge.copyWarriorsToCore();
        gameLoop = new ServerGameLoop(50, pge.getInitialWarriorList(), pge.getPlayground(), 10);

        gameLoop.gameLoop();
        Thread.sleep(11 * 50);

        GameResult result = gameLoop.getGameResult();
        assertEquals("player count", result.getPlayerCount(), 1);
        assertEquals("round Number", result.getRoundNumber(),
                gameLoop.getGameEngine().getGameInfo().getRoundNumber());
        assertFalse("is draw", result.isDraw());
    }

    @Test
    public void testGameLoop_movDraw2() throws InterruptedException {
        list1 = rcParser.parse("MOV 0 1 \n");
        list2 = rcParser.parse("MOV 0 1 \n");
        warrior1 = new Warrior("w1", new WarriorProcess(), list1);
        warrior2 = new Warrior("w2", new WarriorProcess(), list2);

        PreGameEngine pge = new PreGameEngine(2, 2, new Core(5));
        pge.setInitialWarriorList(warrior1, warrior2);
        pge.copyWarriorsToCore();
        gameLoop = new ServerGameLoop(50, pge.getInitialWarriorList(), pge.getPlayground(), 10);

        gameLoop.gameLoop();
        Thread.sleep(11 * 50);

        GameResult result = gameLoop.getGameResult();

        assertEquals("player count", result.getPlayerCount(), 2);
        assertEquals("round Number", result.getRoundNumber(), 50);
        assertTrue("is draw", result.isDraw());
    }

    @Test
    public void testGameLoop_noDraw2() throws InterruptedException {
        list1 = rcParser.parse("MOV 0 1 \n");
        list2 = rcParser.parse("DAT #0 #1 \n");
        warrior1 = new Warrior("w1", new WarriorProcess(), list1);
        warrior2 = new Warrior("w2", new WarriorProcess(), list2);

        PreGameEngine pge = new PreGameEngine(2, 2, new Core(800));
        pge.setInitialWarriorList(warrior1, warrior2);
        pge.copyWarriorsToCore();
        gameLoop = new ServerGameLoop(50, pge.getInitialWarriorList(), pge.getPlayground(), 10);

        gameLoop.gameLoop();
        Thread.sleep(11 * 50);

        GameResult result = gameLoop.getGameResult();
        assertEquals("player count", result.getPlayerCount(), 2);
        assertEquals("round Number", result.getRoundNumber(),
                gameLoop.getGameEngine().getGameInfo().getRoundNumber());
        assertFalse("is draw", result.isDraw());
    }

    // TODO: tests execution
}
