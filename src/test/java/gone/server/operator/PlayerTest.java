package gone.server.operator;

import static org.junit.Assert.assertEquals;

import gone.server.contracts.Player;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.InetAddress;
import java.net.UnknownHostException;

import java.time.Duration;



/**
 * Created by Ferdinand on 22.09.16.
 */
public class PlayerTest {
    private Player player;

    @Before
    public void preparePlayerTest() {
        player = new Player("name", "sessionId1", true, 1, 2, true);

    }

    @Test
    public void getterTests() {
        assertEquals(player.getName(), "name");
        assertEquals(player.getSessionId(), "sessionId1");
        assertEquals(player.isConnected(), true);
        assertEquals(player.getConnectedTimer(), Duration.ZERO);
        assertEquals(player.getPlayedGames(), 1);
        assertEquals(player.getWaitedGames(), 2);
        assertEquals(player.isReady(), true);
    }

    @Test
    public void setterTests() {
        player.setName("newName");
        assertEquals(player.getName(), "newName");

        player.setSessionId("sessionId2");
        assertEquals(player.getSessionId(), "sessionId2");

        player.setConnected(false);
        assertEquals(player.isConnected(), false);

        player.setConnectedTimer(Duration.ofSeconds(10));
        assertEquals(player.getConnectedTimer(), Duration.ofSeconds(10));

        player.setPlayedGames(50);
        assertEquals(player.getPlayedGames(), 50);

        player.setWaitedGames(35);
        assertEquals(player.getWaitedGames(), 35);

        player.setReady(false);
        assertEquals(player.isReady(), false);
    }

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testSetConnectedTimerException() {
        thrown.expect(IllegalArgumentException.class);
        player.setConnectedTimer(Duration.ofSeconds(-13));
    }

    @Test
    public void testSetPlayedGamesException() {
        thrown.expect(IllegalArgumentException.class);
        player.setPlayedGames(-2);
    }

    @Test
    public void testSetWaitedGamesException() {
        thrown.expect(IllegalArgumentException.class);
        player.setWaitedGames(-1);
    }

}
