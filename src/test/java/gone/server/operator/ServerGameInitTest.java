package gone.server.operator;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

/**
 * Created by matthias on 25.08.16.
 */
public class ServerGameInitTest {

    /* error cases */

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testServerGameInit_errorEmptyWarriorCode() {
        String empty = "";
        String name = "empty";
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(empty, name, 10);
    }

    @Test
    public void testServerGameInit_errorEmptyWarriorName() {
        String empty = "JMP 0";
        String name = "";
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(empty, name, 10);
    }

    @Test
    public void testServerGameInit_errorFalseWarriorCode() {
        String a = "a";
        String name = "empty";
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(a, name, 10);
    }

    @Test
    public void testServerGameInit_errorEmptyCore() {
        String mov = "MOV 0 1";
        String name = "mov";
        // core to small (empty)
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(mov, name, 0);
    }

    @Test
    public void testServerGameInit_errorSmallCoreOnePlayer() {
        String dwarf = "ADD #4 3 \n MOV 2 @2 \n JMP -2 \n DAT #0 #0";
        String name = "dwarf";
        // core to small
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(dwarf, name, 3);
    }

    @Test
    public void testServerGameInit_errorSmallCoreTwoPlayers() {
        String dwarf = "ADD #4 3 \n MOV 2 @2 \n JMP -2 \n DAT #0 #0";
        String name1 = "dwarf";
        String mov = "MOV 0 1";
        String name2 = "mov";
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(dwarf, name1, mov, name2, 4);
    }

    @Test
    public void testServerGameInit_errorSameName() {
        String jmp1 = "JMP 0";
        String name1 = "jmp";
        String jmp2 = "JMP 0";
        String name2 = "jmp";
        thrown.expect(IllegalArgumentException.class);
        ServerGameInit gameInit = new ServerGameInit(jmp1, name1, jmp2, name2, 10);
    }

    /* normal cases */

    // help function to test CoreElements
    private void checkCoreElement(Core playground, int index, Class<?> instruction,
                                  FieldPrefix fieldPrefixA, int addressA,
                                  FieldPrefix fieldPrefixB, int addressB) {
        if (playground.getCoreElement(index) instanceof CoreElementInstruction) {
            assertTrue("is instruction", playground.getCoreElement(index)
                    instanceof CoreElementInstruction);
            assertThat("instruction type",
                    ((CoreElementInstruction) playground.getCoreElement(index)).getInstruction(),
                    instanceOf(instruction));
        } else {
            assertTrue("is dat", playground.getCoreElement(index) instanceof CoreElementDat);
        }
        assertEquals("a fieldPrefix",
                fieldPrefixA, playground.getCoreElement(index).getAField().getFieldPrefix());
        assertEquals("a address",
                addressA, playground.getCoreElement(index).getAField().getAddress());
        assertEquals("b fieldPrefix",
                fieldPrefixB, playground.getCoreElement(index).getBField().getFieldPrefix());
        assertEquals("b address",
                addressB, playground.getCoreElement(index).getBField().getAddress());
    }

    @Test
    public void testServerGameInit_onePlayer() {
        String dwarf = "ADD #4 3 \n MOV 2 @2 \n JMP -2 \n DAT #0 #0";
        String name = "dwarf";
        ServerGameInit gameInit = new ServerGameInit(dwarf, name, 10);

        // test warrior
        Warrior dwarfWarr = gameInit.getPreGameE().getInitialWarriorList().get(0);
        assertEquals("warr list size", gameInit.getPreGameE().getInitialWarriorList().size(), 1);
        assertEquals("max player count", PreGameEngine.getMaxPlayerCount(), 1);
        assertEquals("warrior name", dwarfWarr.getName(), "dwarf");
        assertEquals("instruction count", dwarfWarr.getInstructionsCount(), 4);
        assertTrue("alive", dwarfWarr.getIsAlive());
        int dwarfStartPos = dwarfWarr.getStartAddressAtCore();
        assertTrue("dwarf start pos", dwarfStartPos >= 0
                && dwarfStartPos <= gameInit.getPreGameE().getPlayground().getSize());

        // test start player
        int startPlayerCount = 0;
        for (int i = 0; i < gameInit.getPreGameE().getInitialWarriorList().size(); i++) {
            if (gameInit.getPreGameE().getInitialWarriorList().get(i).isStartPlayer()) {
                startPlayerCount++;
            }
        }
        assertEquals("start player count" , startPlayerCount, 1);

        // test core
        assertEquals("core size pge", gameInit.getPreGameE().getPlayground().getSize(), 10);
        Core playground = gameInit.getInitiatedCore();
        assertEquals("core size pge", playground.getSize(), 10);

        // dwarf
        checkCoreElement(playground, dwarfStartPos, AddInstruction.class, FieldPrefix.Immediate,
                4, FieldPrefix.Direct, 3);
        checkCoreElement(playground, dwarfStartPos + 1, MoveInstruction.class, FieldPrefix.Direct,
                2, FieldPrefix.Indirect, 2);
        checkCoreElement(playground, dwarfStartPos + 2, JumpInstruction.class, FieldPrefix.Direct,
                -2, FieldPrefix.Direct, 0);
        checkCoreElement(playground, dwarfStartPos + 3, null, FieldPrefix.Immediate,
                0, FieldPrefix.Immediate, 0);

        // rest DAT 0 0
        for (int i = 0; i < 6; i++) {
            checkCoreElement(playground, dwarfStartPos + 4 + i, null, FieldPrefix.Direct,
                    0, FieldPrefix.Direct, 0);
        }
    }

    @Test
    public void testServerGameInit_twoPlayers() {
        String code1 = "MOV 0 1 \n ADD #0 #1";
        String player1 = "player1";
        String code2 = "ADD #0 #1 \n MOV 0 1 \n JMP 0";
        String player2 = "player2";
        ServerGameInit gameInit = new ServerGameInit(code1, player1, code2, player2, 20);

        // test warrior
        Warrior warrior1 = gameInit.getPreGameE().getInitialWarriorList().get(0);
        Warrior warrior2 = gameInit.getPreGameE().getInitialWarriorList().get(1);
        assertEquals("warr list size", gameInit.getPreGameE().getInitialWarriorList().size(), 2);
        assertEquals("max player count", PreGameEngine.getMaxPlayerCount(), 2);

        // test start player
        int startPlayerCount = 0;
        int notStartPlayerCount = 0;
        for (int i = 0; i < gameInit.getPreGameE().getInitialWarriorList().size(); i++) {
            if (gameInit.getPreGameE().getInitialWarriorList().get(i).isStartPlayer()) {
                startPlayerCount++;
            } else {
                notStartPlayerCount++;
            }
        }
        assertEquals("start player count" , startPlayerCount, 1);
        assertEquals("not start player count" , notStartPlayerCount, 1);

        // test names - random init. (start player)
        int player1Name = 0;
        int player2Name = 0;
        int elseCount = 0;
        for (int i = 0; i < gameInit.getPreGameE().getInitialWarriorList().size(); i++) {
            if (gameInit.getPreGameE().getInitialWarriorList().get(i).getName().equals("player1")) {
                player1Name++;
            } else if (gameInit.getPreGameE().getInitialWarriorList().get(i).getName().equals("player2")) {
                player2Name++;
            } else {
                elseCount++;
            }
        }
        assertEquals("name player1 count", player1Name, 1);
        assertEquals("name player2 count", player2Name, 1);
        assertEquals("name else count", elseCount, 0);

        assertTrue("instruction count index 0",
                warrior1.getInstructionsCount() == 2 || warrior1.getInstructionsCount() == 3);
        assertTrue("instruction count index 1",
                warrior2.getInstructionsCount() == 2 || warrior2.getInstructionsCount() == 3);
        assertTrue("instruction count not equal",
                warrior1.getInstructionsCount() != warrior2.getInstructionsCount());
        assertTrue("alive 1", warrior1.getIsAlive());
        assertTrue("alive 2", warrior2.getIsAlive());

        // starting positions at core
        int warrior1StartPos = warrior1.getStartAddressAtCore();
        assertTrue("warrior1 start pos", warrior1StartPos >= 0
                && warrior1StartPos <= gameInit.getPreGameE().getPlayground().getSize());
        int warrior2StartPos = warrior2.getStartAddressAtCore();
        assertTrue("warrior2 start pos", warrior2StartPos >= 0
                && warrior2StartPos <= gameInit.getPreGameE().getPlayground().getSize());
        assertTrue("start pos not equal", warrior1StartPos != warrior2StartPos);

        // test core
        assertEquals("core size pge", gameInit.getPreGameE().getPlayground().getSize(), 20);
        Core playground = gameInit.getInitiatedCore();
        assertEquals("core size pge", playground.getSize(), 20);

        // calculate the list index of each player -> then get start pos at core from that info
        List<Warrior> pgeInitList = gameInit.getPreGameE().getInitialWarriorList();
        int listIndexMovFirst = 0;
        int listIndexAddFirst = 0;
        int movFirstInstructionStartPos; // player has MOV 0 1 as first instruction
        int addFirstInstructionStartPos; // player has ADD #0 #1 as first instruction

        for (int i = 0; i < gameInit.getPreGameE().getInitialWarriorList().size(); i++) {
            if (gameInit.getPreGameE().getInitialWarriorList().get(i).getRcProgram()
                    .get(0).getOperationCode().equals("MOV")) {
                listIndexMovFirst = i;
            }
            if (gameInit.getPreGameE().getInitialWarriorList().get(i).getRcProgram()
                    .get(0).getOperationCode().equals("ADD")) {
                listIndexAddFirst = i;
            }
        }
        movFirstInstructionStartPos = pgeInitList.get(listIndexMovFirst).getStartAddressAtCore();
        addFirstInstructionStartPos = pgeInitList.get(listIndexAddFirst).getStartAddressAtCore();
        assertTrue("start pos not equal", movFirstInstructionStartPos != addFirstInstructionStartPos);

        // test code 1
        checkCoreElement(playground, movFirstInstructionStartPos, MoveInstruction.class,
                FieldPrefix.Direct, 0, FieldPrefix.Direct, 1);
        checkCoreElement(playground, movFirstInstructionStartPos + 1, AddInstruction.class,
                FieldPrefix.Immediate, 0, FieldPrefix.Immediate, 1);

        // test code 2
        checkCoreElement(playground, addFirstInstructionStartPos, AddInstruction.class,
                FieldPrefix.Immediate, 0, FieldPrefix.Immediate, 1);
        checkCoreElement(playground, addFirstInstructionStartPos + 1, MoveInstruction.class,
                FieldPrefix.Direct, 0, FieldPrefix.Direct, 1);
        checkCoreElement(playground, addFirstInstructionStartPos + 2, JumpInstruction.class,
                FieldPrefix.Direct, 0, FieldPrefix.Direct, 0);

        // rest DAT 0 0
        int datCountRest = 0;
        for (int i = 0; i < playground.getSize(); i++) {
            CoreElement toCheck = playground.getCoreElement(i);
            if (toCheck.getAField().getAddress() == 0
                    && toCheck.getAField().getFieldPrefix() == FieldPrefix.Direct
                    && toCheck.getBField().getAddress() == 0
                    && toCheck.getBField().getFieldPrefix() == FieldPrefix.Direct
                    && toCheck instanceof CoreElementDat) {
                checkCoreElement(playground, i, null, FieldPrefix.Direct, 0, FieldPrefix.Direct, 0);
                datCountRest++;
            }
        }
        assertEquals("rest dat count", datCountRest, 15);
    }
}
