package gone.server.parser;

import static org.junit.Assert.assertEquals;

import gone.server.contracts.FieldPrefix;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Winfried on 28.06.2016.
 * This test class contains several tests for the parser and checks special situations
 */
public class RedcodeSyntaxTest {
    private RedcodeParser sut;
    private List<InstructionTemplate> instructionTemplates;

    @Before
    public void setUp() {
        sut = new RedcodeParser();
    }

    /**
     * Feeds the parser with one operation including multiple white spaces
     */
    @Test
    public void testMultipleSpaces() {
        instructionTemplates = sut.parse("ADD        <14       @3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "ADD");

        assertEquals("mode A", FieldPrefix.IndirectWithPredecrement, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Indirect, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    /**
     * Feeds the parser with three operations separated by different line endings
     */
    @Test
    public void testNewLine() {
        instructionTemplates = sut.parse("ADD <14 @3\nMOV #33 @25\r\nSUB <14 #3");

        assertEquals("three operations", instructionTemplates.size(), 3);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "ADD");

        assertEquals("mode A", FieldPrefix.IndirectWithPredecrement, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Indirect, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());

        template = instructionTemplates.get(1);

        assertEquals("operation code", template.getOperationCode(), "MOV");

        assertEquals("mode A", FieldPrefix.Immediate, template.getAField().getFieldPrefix());
        assertEquals("value A", 33, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Indirect, template.getBField().getFieldPrefix());
        assertEquals("value B", 25, template.getBField().getAddress());

        template = instructionTemplates.get(2);

        assertEquals("operation code", template.getOperationCode(), "SUB");

        assertEquals("mode A", FieldPrefix.IndirectWithPredecrement, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    /**
     * Feeds the parser with two operations separated by new Line including multiple white spaces
     */
    @Test
    public void testMultipleSpacesAndNewLine() {
        instructionTemplates = sut.parse("JMZ     <55      @3\r\nMOV      #33      <22");

        assertEquals("two operations", instructionTemplates.size(), 2);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "JMZ");

        assertEquals("mode A", FieldPrefix.IndirectWithPredecrement, template.getAField().getFieldPrefix());
        assertEquals("value A", 55, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Indirect, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());

        template = instructionTemplates.get(1);

        assertEquals("operation code", template.getOperationCode(), "MOV");

        assertEquals("mode A", FieldPrefix.Immediate, template.getAField().getFieldPrefix());
        assertEquals("value A", 33, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.IndirectWithPredecrement, template.getBField().getFieldPrefix());
        assertEquals("value B", 22, template.getBField().getAddress());
    }

    /**
     * Checks, whether the Parser throws an exception if an invalid prefix occurs for argument A
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDirectPrefixA() {
        sut.parse("ADD $14 3");
    }

    /**
     * Checks, whether the Parser throws an exception if an invalid prefix occurs for argument B
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDirectPrefixB() {
        sut.parse("ADD 14 $3");
    }

}
