package gone.server.parser;

import static org.junit.Assert.assertEquals;

import gone.server.contracts.FieldPrefix;

import org.junit.Before;
import org.junit.Test;

import java.util.List;


/**
 * Created by Winfried and Stenley on 13.06.2016.
 */

public class RedcodeParserTest {
    private RedcodeParser sut;
    private List<InstructionTemplate> instructionTemplates;

    @Before
    public void setUp() {
        sut = new RedcodeParser();
    }

    @Test
    public void parseInstructionDat() throws Exception {
        instructionTemplates = sut.parse("dat #3 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "DAT");

        assertEquals("mode A", FieldPrefix.Immediate, template.getAField().getFieldPrefix());
        assertEquals("value A", 3, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionMov() throws Exception {
        instructionTemplates = sut.parse("mov 14 @300");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "MOV");

        assertEquals("mode A", FieldPrefix.Direct, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Indirect, template.getBField().getFieldPrefix());
        assertEquals("value B", 300, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionAdd() throws Exception {
        instructionTemplates = sut.parse("ADD @14 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "ADD");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionSub() throws Exception {
        instructionTemplates = sut.parse("sub @14 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "SUB");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 14, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionJmp() throws Exception {
        instructionTemplates = sut.parse("jmp @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "JMP");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionJmz() throws Exception {
        instructionTemplates = sut.parse("jmz @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "JMZ");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionJmn() throws Exception {
        instructionTemplates = sut.parse("jmn @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "JMN");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionDjn() throws Exception {
        instructionTemplates = sut.parse("djn @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "DJN");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionCmp() throws Exception {
        instructionTemplates = sut.parse("cmp @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "CMP");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionSlt() throws Exception {
        instructionTemplates = sut.parse("slt @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "SLT");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }

    @Test
    public void parseInstructionSpl() throws Exception {
        instructionTemplates = sut.parse("spl @140 #3");

        assertEquals("one operation", instructionTemplates.size(), 1);

        InstructionTemplate template = instructionTemplates.get(0);

        assertEquals("operation code", template.getOperationCode(), "SPL");

        assertEquals("mode A", FieldPrefix.Indirect, template.getAField().getFieldPrefix());
        assertEquals("value A", 140, template.getAField().getAddress());

        assertEquals("mode B", FieldPrefix.Immediate, template.getBField().getFieldPrefix());
        assertEquals("value B", 3, template.getBField().getAddress());
    }
}
