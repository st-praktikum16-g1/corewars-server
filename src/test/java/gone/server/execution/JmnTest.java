package gone.server.execution;

import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class JmnTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //changes the instruction pointer to where JMN.A points to
    //if the B-field where JMN.B points to is != 0
    //here: jump
    //verified with CoreWin
    public void testJmn_1() {
        String warriorCode = String.join("\n",
                "JMN 2 1",
                "DAT #0 #9",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //changes the instruction pointer to where JMN.A points to
    //if the B-field where JMN.B points to is != 0
    //here: don't jump
    //validated with CoreWin
    public void testJmn_2() {
        String warriorCode = String.join("\n",
                "JMN 2 0",
                "DAT #0 #0",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
    }


    @Test
    //here: jump
    // validated with CoreWin
    public void testJmn_3() {
        String warriorCode = String.join("\n",
                "JMN 2, #1",
                "DAT #0, #0",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }
}
