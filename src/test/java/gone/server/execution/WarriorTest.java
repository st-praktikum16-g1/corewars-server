package gone.server.execution;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.FieldPrefix;

import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;

import gone.server.gameengine.GameEngine;
import gone.server.gameengine.GameInfo;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;

import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import org.junit.Test;

import java.util.List;


/**
 * Created by benedikt8 on 28.06.16.
 */
public class WarriorTest {
    // This Integration Test is based on the requirement in the requirement specification
    // (Lastenheft) that a Imp must work.
    private Core core;
    private WarriorProcess process;
    private PreGameEngine preGameEngine;
    private GameEngine gameEngineE;
    private GameInfo gameInfo;

    private static final FieldPrefix DIRECT = FieldPrefix.Direct;
    private static final FieldPrefix IMMEDIATE = FieldPrefix.Immediate;
    private static final FieldPrefix INDIRECT = FieldPrefix.Indirect;

    // help function to test CoreElements
    private void checkCoreElement(Core playground, int index, Class<?> instruction,
                                  FieldPrefix fieldPrefixA, int addressA,
                                  FieldPrefix fieldPrefixB, int addressB) {
        if (playground.getCoreElement(index) instanceof CoreElementInstruction) {
            assertTrue("is instruction", playground.getCoreElement(index)
                    instanceof CoreElementInstruction);
            assertThat("instruction type",
                    ((CoreElementInstruction) playground.getCoreElement(index)).getInstruction(),
                    instanceOf(instruction));
        } else {
            assertTrue("is dat", playground.getCoreElement(index) instanceof CoreElementDat);
        }
        assertEquals("a fieldPrefix",
                fieldPrefixA, playground.getCoreElement(index).getAField().getFieldPrefix());
        assertEquals("a address",
                addressA, playground.getCoreElement(index).getAField().getAddress());
        assertEquals("b fieldPrefix",
                fieldPrefixB, playground.getCoreElement(index).getBField().getFieldPrefix());
        assertEquals("b address",
                addressB, playground.getCoreElement(index).getBField().getAddress());
    }

    private void prepareImp() {
        preGameEngine = new PreGameEngine(2, 1, new Core(4));
        List<InstructionTemplate> list1 = new RedcodeParser().parse("MOV 0 1");
        Warrior warrior1 = new Warrior("mov01", new WarriorProcess(), list1);
        preGameEngine.setInitialWarriorList(warrior1);
        preGameEngine.copyWarriorsToCore();
        gameEngineE = new GameEngine(100, preGameEngine.getInitialWarriorList(),
                preGameEngine.getPlayground());
    }

    @Test
    public void test_imp() {
        int rounds = 4;
        prepareImp();
        int movStart = preGameEngine.getInitialWarriorList().get(0).getStartAddressAtCore();

        // let the imp walk through the whole core
        for (int i = 0; i < rounds; i++) {
            Core playground = gameEngineE.getPlayCore();

            // mov fields
            for (int j = 0; j <= i; j++) {
                checkCoreElement(playground, movStart + j, MoveInstruction.class,
                        DIRECT, 0, DIRECT, 1);
            }

            // the remaining elements are supposed to stay "DAT 0 0"
            for (int j = 0; j < (playground.getSize() - i - 1); j++) {
                checkCoreElement(playground, movStart + j + 1 + i, null,
                        DIRECT, 0, DIRECT, 0);
            }

            // DO EXECUTION
            gameEngineE.gameStep();
        }
    }

    private void prepareDwarf() {
        preGameEngine = new PreGameEngine(8, 1, new Core(8));
        List<InstructionTemplate> dwarf = new RedcodeParser().parse(
                "ADD #4 3 \n MOV 2 @2 \n JMP -2 \n DAT #0 #0");
        Warrior warrior1 = new Warrior("dwarf", new WarriorProcess(), dwarf);
        preGameEngine.setInitialWarriorList(warrior1);
        preGameEngine.copyWarriorsToCore();
        gameEngineE = new GameEngine(100, preGameEngine.getInitialWarriorList(),
                preGameEngine.getPlayground());
    }

    /**
     * Core content depending on cycle and position.
     * 0: ADD #4, 3
     * 1: MOV 2, @2
     * 2: JMP -2
     * 3: DAT #0, #[4*(cycle-1)] if position == 0; else [4 * cycle]
     * 4: DAT 0, 0
     * 5: DAT 0, 0
     * 6: DAT 0, 0
     * 7: if (cycle == 1 && position < 2): DAT 0, 0
     * 7:   else if (position < 2): DAT #0, #[4*(cycle-1)]
     * 7:       else: DAT #0, #[4 * cycle]
     */
    @Test
    public void test_dwarf() {
        prepareDwarf();
        int dwarfStart = gameEngineE.getWarriorRunQueue().getWarriorRunQueue()
                .element().getStartAddressAtCore();

        // execute on line of the dwarf code each cycle (3 instructions)
        for (int i = 0; i < 9; i++) {
            // DO EXECUTION
            gameEngineE.gameStep();
            Core playground = gameEngineE.getPlayCore();

            checkCoreElement(playground, dwarfStart, AddInstruction.class,
                    IMMEDIATE, 4, DIRECT, 3);
            checkCoreElement(playground, dwarfStart + 1, MoveInstruction.class,
                    DIRECT, 2, INDIRECT, 2);
            checkCoreElement(playground, dwarfStart + 2, JumpInstruction.class,
                    DIRECT, -2, DIRECT, 0);

            // 3: DAT #0 #x
            if (i >= 0 && i <= 2) {
                checkCoreElement(playground, dwarfStart + 3, null,
                        IMMEDIATE, 0, IMMEDIATE, 4);
            }
            if (i >= 3 && i <= 5) {
                checkCoreElement(playground, dwarfStart + 3, null,
                        IMMEDIATE, 0, IMMEDIATE, 8);
            }
            if (i >= 6) {
                checkCoreElement(playground, dwarfStart + 3, null,
                        IMMEDIATE, 0, IMMEDIATE, 12);
            }

            // 4-6: stays DAT 0 0
            for (int j = 4; j <= 6; j++) {
                checkCoreElement(playground, dwarfStart + j, null,
                        DIRECT, 0, DIRECT, 0);
            }

            // 7: DAT #0 #y
            if (i == 0) {   // init DAT 0 0
                checkCoreElement(playground, dwarfStart + 7, null,
                        DIRECT, 0, DIRECT, 0);
            }
            if (i >= 1 && i <= 6) {
                checkCoreElement(playground, dwarfStart + 7, null,
                        IMMEDIATE, 0, IMMEDIATE, 4);
            }
            if (i >= 7) {
                checkCoreElement(playground, dwarfStart + 7, null,
                        IMMEDIATE, 0, IMMEDIATE, 12);
            }
        }
    }
}
