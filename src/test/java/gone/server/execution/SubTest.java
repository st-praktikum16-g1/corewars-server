package gone.server.execution;

import static gone.server.execution.CoreCompareAfterOneExecution.compareCoresAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 18.08.2016.
 */
public class SubTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //A immediate -> subtract the value from ADD.A from the value of the B-field
    //               from where ADD.B points to
    public void testSub_1() {
        String codeBefore = String.join("\n",
                "SUB #2 1",
                "DAT #0 #5",
                "");
        String codeWanted = String.join("\n",
                "SUB #2 1",
                "DAT #0 #3",
                "");
        compareCoresAfterOneExecution(2, codeBefore, codeWanted, false);
    }

    @Test
    //A not immediate -> subtract A and B from where ADD.A points to,
    //                   from A respectively B where ADD.B points to
    public void testSub_2() {
        String codeBefore = String.join("\n",
                "SUB 1 2",
                "DAT #1 #2",
                "DAT #4 #6",
                "");
        String codeWanted = String.join("\n",
                "SUB 1 2",
                "DAT #1 #2",
                "DAT #3 #4",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

}
