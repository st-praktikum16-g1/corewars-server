package gone.server.execution;

import static org.junit.Assert.assertEquals;

import gone.server.core.Core;
import gone.server.gameengine.GameEngine;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import java.util.List;

/**
 * Created by Ferdinand on 19.08.2016.
 */
class InstructionPointerCompareAfterOneExecution {
    static void compareIpAfterOneExecution(int coreSize, int expectedIpOffset, String warriorCode) {

        //core that will be executed
        PreGameEngine preGameEngineToTest = new PreGameEngine(coreSize, 1, new Core(coreSize));

        List<InstructionTemplate> listBefore = new RedcodeParser().parse(warriorCode);
        Warrior warriorToTest = new Warrior("Before", new WarriorProcess(), listBefore);
        preGameEngineToTest.setInitialWarriorList(warriorToTest);
        preGameEngineToTest.copyWarriorsToCore();
        GameEngine gameEngineToTest = new GameEngine(100,
                preGameEngineToTest.getInitialWarriorList(), preGameEngineToTest.getPlayground());

        int startIp = warriorToTest.getStartAddressAtCore();

        //execute one step
        gameEngineToTest.gameStep();

        int newIp = warriorToTest.getWarriorProcessQueue().getCurrent().getInstructionPointer();

        assertEquals(expectedIpOffset, newIp - startIp);
    }

    static void compareIpAfterTwoExecutions(int coreSize, int expectedIpOffset, String warriorCode) {

        //core that will be executed
        PreGameEngine preGameEngineToTest = new PreGameEngine(coreSize, 1, new Core(coreSize));

        List<InstructionTemplate> listBefore = new RedcodeParser().parse(warriorCode);
        Warrior warriorToTest = new Warrior("Before", new WarriorProcess(), listBefore);
        preGameEngineToTest.setInitialWarriorList(warriorToTest);
        preGameEngineToTest.copyWarriorsToCore();
        GameEngine gameEngineToTest = new GameEngine(100,
                preGameEngineToTest.getInitialWarriorList(), preGameEngineToTest.getPlayground());

        int startIp = warriorToTest.getStartAddressAtCore();

        //execute one step
        gameEngineToTest.gameStep();
        //and another one
        gameEngineToTest.gameStep();

        int newIp = warriorToTest.getWarriorProcessQueue().getCurrent().getInstructionPointer();

        assertEquals(expectedIpOffset, newIp - startIp);
    }
}
