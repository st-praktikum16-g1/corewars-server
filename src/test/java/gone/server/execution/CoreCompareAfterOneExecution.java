package gone.server.execution;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import gone.server.contracts.CoreElement;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.gameengine.GameEngine;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;

import java.util.List;

/**
 * Created by Ferdinand on 18.08.2016.
 */
class CoreCompareAfterOneExecution {
    static void compareCoresAfterOneExecution(int coreSize, String codeBefore, String codeWanted, boolean forcePosition) {
        int positionInCore;
        PreGameEngine preGameEngineForExecution;
        Warrior warriorForExecution;

        do {
            //core that will be executed
            preGameEngineForExecution = new PreGameEngine(coreSize, 1, new Core(coreSize));
            List<InstructionTemplate> listBefore = new RedcodeParser().parse(codeBefore);
            warriorForExecution = new Warrior("forExecution", new WarriorProcess(), listBefore);
            preGameEngineForExecution.setInitialWarriorList(warriorForExecution);
            preGameEngineForExecution.copyWarriorsToCore();
            positionInCore = preGameEngineForExecution.getStartPositionByWarrior(warriorForExecution);
        }
        while ((forcePosition) == (positionInCore != 0));

        GameEngine gameEngineForExecution = new GameEngine(100,
                preGameEngineForExecution.getInitialWarriorList(),
                preGameEngineForExecution.getPlayground());

        Core coreForExecution = gameEngineForExecution.getPlayCore();

        int executionStartAddress = warriorForExecution.getStartAddressAtCore();

        //expected CoreElements
        PreGameEngine preGameEngineForComparison = new PreGameEngine(coreSize, 1, new Core(coreSize));
        List<InstructionTemplate> listWanted = new RedcodeParser().parse(codeWanted);
        Warrior warriorForComparison = new Warrior("forComparison", new WarriorProcess(), listWanted);
        preGameEngineForComparison.setInitialWarriorList(warriorForComparison);
        preGameEngineForComparison.copyWarriorsToCore();
        GameEngine gameEngineForCompare = new GameEngine(100,
                preGameEngineForComparison.getInitialWarriorList(),
                preGameEngineForComparison.getPlayground());

        Core coreForComparison = gameEngineForCompare.getPlayCore();

        int comparisonStartAddress = warriorForComparison.getStartAddressAtCore();

        //run one execution step
        gameEngineForExecution.gameStep();

        //gameEngineForExecution.getPlayCore().printCore();

        //compare if the CoreElements in the core after execution match the expected CoreElements
        for (int i = 0; i < coreSize; ++i) {
            assertCoreElementsEqual((coreForComparison.getCoreElement(comparisonStartAddress + i)),
                    (coreForExecution.getCoreElement(executionStartAddress + i)));
        }

    }

    private static void assertCoreElementsEqual(CoreElement firstCoreElement, CoreElement secondCoreElement) {
        assertEquals("CoreElement Type", firstCoreElement == instanceOf(CoreElementDat.class),
                secondCoreElement == instanceOf(CoreElementDat.class));
        if (firstCoreElement == instanceOf(CoreElementInstruction.class)) {
            assertEquals("Instruction Type",
                    ((CoreElementInstruction) firstCoreElement).getInstruction(),
                    ((CoreElementInstruction) secondCoreElement).getInstruction());
        }
        assertEquals("A-Field Prefix", firstCoreElement.getAField().getFieldPrefix(),
                secondCoreElement.getAField().getFieldPrefix());
        assertEquals("A-Field Address", firstCoreElement.getAField().getAddress(),
                secondCoreElement.getAField().getAddress());
        assertEquals("B-Field Prefix", firstCoreElement.getBField().getFieldPrefix(),
                secondCoreElement.getBField().getFieldPrefix());
        assertEquals("B-Field Address", firstCoreElement.getBField().getAddress(),
                secondCoreElement.getBField().getAddress());
    }
}
