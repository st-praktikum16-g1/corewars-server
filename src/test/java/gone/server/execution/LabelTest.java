package gone.server.execution;

import org.junit.Test;

/**
 * tests labels.
 * Created by Ferdinand on 25.09.2016.
 */

public class LabelTest {
    @Test
    //validated with CoreWin and ARES
    public void testLabel_1() {
        String codeBefore = String.join("\n",
                "labl ADD lebl lebl",
                "lebl DAT #0 #3",
                "");
        String codeWanted = String.join("\n",
                "ADD 1 1",
                "DAT #0 #6",
                "");
        CoreCompareAfterOneExecution.compareCoresAfterOneExecution(2, codeBefore, codeWanted, true);
    }
}
