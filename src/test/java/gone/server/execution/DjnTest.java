package gone.server.execution;

import static gone.server.execution.CoreCompareAfterOneExecution.compareCoresAfterOneExecution;
import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class DjnTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //changes the instruction pointer to where DJN.A points to
    //if the B-field where DJN.B points to decremented is != 0
    //here: jump
    public void testDjn_1() {
        String warriorCode = String.join("\n",
                "DJN 2 1",
                "DAT #0 #5",
                "");
        String codeWanted = String.join("\n",
                "DJN 2 1",
                "DAT #0 #4",
                "");
        //test if instructionPointer is right
        compareIpAfterOneExecution(3, 2, warriorCode);
        // test if core is right
        compareCoresAfterOneExecution(3, warriorCode, codeWanted, false);
    }

    @Test
    //changes the instruction pointer to where DJN.A points to
    //if the B-field where DJN.B points to decremented is != 0
    //here: don't jump
    public void testDjn_2() {
        String warriorCode = String.join("\n",
                "DJN 2 1",
                "DAT #0 #1",
                "");
        String codeWanted = String.join("\n",
                "DJN 2 1",
                "DAT #0 #0",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
        compareCoresAfterOneExecution(3, warriorCode, codeWanted, false);
    }

    @Test
    //here: jump
    //tested with CoreWin
    //tests fails because indirect with predecrement doesn't work
    public void testDjn_3() {
        String warriorCode = String.join("\n",
                "DJN 2 <1",
                "DAT #0 #1",
                "DAT #0 #0",
                "");
        String codeWanted = String.join("\n",
                "DJN 2 <1",
                "DAT #0 #-1",
                "DAT #0 #0",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
        compareCoresAfterOneExecution(3, warriorCode, codeWanted, false);
    }

    @Test
    //here: jump
    //tested with CoreWin
    //tests fails because indirect with predecrement doesn't work
    public void testDjn_4() {
        String warriorCode = String.join("\n",
                "DJN 2 <1",
                "DAT #0 #2",
                "DAT #0 #0",
                "");
        String codeWanted = String.join("\n",
                "DJN 2 <1",
                "DAT #0 #1",
                "DAT #0 #-1",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
        compareCoresAfterOneExecution(3, warriorCode, codeWanted, false);
    }

    @Test
    //here: don't jump
    //tested with CoreWin and ARES
    public void testDjn_5() {
        String warriorCode = String.join("\n",
                "DJN 2 @2",
                "DAT #0 #1",
                "DAT #0 #-1",
                "");
        String codeWanted = String.join("\n",
                "DJN 2 @2",
                "DAT #0 #0",
                "DAT #0 #-1",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
        compareCoresAfterOneExecution(3, warriorCode, codeWanted, false);
    }
}
