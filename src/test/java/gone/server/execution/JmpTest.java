package gone.server.execution;

import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class JmpTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //changes the instruction pointer to where JMP.A points to
    public void testJmp_1() {
        String warriorCode = String.join("\n",
                "JMP 2 0",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }
}
