package gone.server.execution;

import org.junit.Test;

/**
 * Created by Ferdinand on 18.08.2016.
 */
public class AddTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //A immediate -> add the value from ADD.A to the value of the B-field from where ADD.B points to
    public void testAdd_1() {
        String codeBefore = String.join("\n",
                "ADD #2 1",
                "DAT #0 #3",
                "");
        String codeWanted = String.join("\n",
                "ADD #2 1",
                "DAT #0 #5",
                "");
        CoreCompareAfterOneExecution.compareCoresAfterOneExecution(2, codeBefore, codeWanted, false);
    }

    @Test
    //A not immediate -> add A and B from where ADD.A points to,
    //                   to A respectively B where ADD.B points to
    public void testAdd_2() {
        String codeBefore = String.join("\n",
                "ADD 1 2",
                "DAT #1 #2",
                "DAT #3 #4",
                "");
        String codeWanted = String.join("\n",
                "ADD 1 2",
                "DAT #1 #2",
                "DAT #4 #6",
                "");
        CoreCompareAfterOneExecution.compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }
}
