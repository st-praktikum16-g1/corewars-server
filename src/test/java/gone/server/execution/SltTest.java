package gone.server.execution;

import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class SltTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //skip the next instruction if A<B
    //here: skip
    public void testSlt_1() {
        String warriorCode = String.join("\n",
                "SLT 1 2",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //here: don't skip
    public void testSlt_2() {
        String warriorCode = String.join("\n",
                "SLT 2 2",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
    }
}
