package gone.server.execution;

import static gone.server.execution.CoreCompareAfterOneExecution.compareCoresAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 16.08.2016.
 */
public class MovTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md


    //test for A and B not immediate
    @Test
    //A and B direct
    public void testMov_0() {
        String codeBefore = String.join("\n",
                "MOV 0 1",
                "");
        String codeWanted = String.join("\n",
                "MOV 0 1",
                "MOV 0 1",
                "");
        compareCoresAfterOneExecution(2, codeBefore, codeWanted, false);
    }

    @Test
    //A and B direct
    public void testMov_1() {
        String codeBefore = String.join("\n",
                "MOV 1 2",
                "DAT #9 #9",
                "");
        String codeWanted = String.join("\n",
                "MOV 1 2",
                "DAT #9 #9",
                "DAT #9 #9",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

    @Test
    //A direct and B indirect
    public void testMov_2() {
        String codeBefore = String.join("\n",
                "MOV 0 @1",
                "DAT #0 #1",
                "");
        String codeWanted = String.join("\n",
                "MOV 0 @1",
                "DAT #0 #1",
                "MOV 0 @1",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

    @Test
    //A predecrement and B direct
    public void testMov_3() {
        String codeBefore = String.join("\n",
                "MOV 0 0",
                "DAT #1 #0",
                "");
        String codeWanted = String.join("\n",
                "MOV 0 0",
                "DAT #1 #0",
                "");
        compareCoresAfterOneExecution(2, codeBefore, codeWanted, false);
    }

    @Test
    //A direct and B predecrement
    public void testMov_4() {
        String codeBefore = String.join("\n",
                "MOV 1 <1",
                "DAT #0 #0",
                "");
        String codeWanted = String.join("\n",
                "DAT #0 #-1",
                "DAT #0 #-1",
                "");
        compareCoresAfterOneExecution(2, codeBefore, codeWanted, false);
    }

    @Test
    //A and B indirect
    //throws a exception in parser IllegalArgumentException("illegal addressing mode")
    //validated with CoreWin and ARES
    //According to the ICWS 88: "MOV @A @B" is allowed
    public void testMov_5_double_indirect() {
        String codeBefore = String.join("\n",
                "MOV #9 @1",
                "eins DAT #3 #1",
                "DAT #5 #1",
                "");
        String codeWanted = String.join("\n",
                "MOV #9 @1",
                "DAT #3 #1",
                "DAT #5 #9",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

    @Test
    //A direct and B predecrement
    //validated with CoreWin and ARES
    public void testMov_6() {
        String codeBefore = String.join("\n",
                "MOV 0 <1",
                "DAT #4 #2",
                "DAT #5 #0",
                "");
        String codeWanted = String.join("\n",
                "MOV 0 <1",
                "DAT #4 #1",
                "MOV 0 <1",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

    //tests for A immediate and B not immediate
    @Test
    //A immediate and B direct
    public void testMov_7() {
        String codeBefore = String.join("\n",
                "MOV #2, 1",
                "DAT #0, #0",
                "DAT #0, #8",
                "");
        String codeWanted = String.join("\n",
                "MOV #2 1",
                "DAT #0 #2",
                "DAT #0 #8",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

    @Test
    public void testMov_8() {
        String codeBefore = String.join("\n",
                "MOV <1 1",
                "DAT #0 #0",
                "");
        String codeWanted = String.join("\n",
                "MOV <1 1",
                "MOV <1 1",
                "");
        compareCoresAfterOneExecution(3, codeBefore, codeWanted, false);
    }

}
