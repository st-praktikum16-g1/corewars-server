package gone.server.execution;

import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;
import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterTwoExecutions;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class CmpTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //compares two fields (if A is immediate) or two entire instructions (if A is not immediate)
    //if they are equal, skips the next instruction

    //here: compare fields, don't skip
    public void testCmp_1() {
        String warriorCode = String.join("\n",
                "CMP #1 0",
                "");
        compareIpAfterOneExecution(5, 1, warriorCode);
    }

    @Test
    //here: compare two DAT 0 0 instructions, skip
    public void testCmp_2() {
        String warriorCode = String.join("\n",
                "CMP 1 2",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //here: compare two DATs with different direct b-addresses, don't skip
    public void testCmp_3() {
        String warriorCode = String.join("\n",
                "ADD #1 2",
                "CMP 1 2",
                "");
        compareIpAfterTwoExecutions(4, 2, warriorCode);
    }

    @Test
    //here: compare different instructions with different addressing types, don't skip
    public void testCmp_4() {
        String warriorCode = String.join("\n",
                "CMP 1 2",
                "CMP #1 #1",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
    }

    @Test
    //here: compare DATs with different addressing modes and addresses, don't skip
    public void testCmp_5() {
        String warriorCode = String.join("\n",
                "CMP 1 2",
                "DAT #1 #1",
                "");
        compareIpAfterOneExecution(4, 1, warriorCode);
    }

    @Test
    public void testCmp_6() {
        String warriorCode = String.join("\n",
                "CMP 0 1",
                "ADD #3 @1",
                //"ADD #5 <6",
                "");
        compareIpAfterOneExecution(4, 1, warriorCode);
    }
}
