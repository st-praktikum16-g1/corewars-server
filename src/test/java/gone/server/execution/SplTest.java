package gone.server.execution;

import static org.junit.Assert.assertEquals;

import gone.server.contracts.IProcess;
import gone.server.core.Core;
import gone.server.gameengine.GameEngine;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.InstructionTemplate;
import gone.server.parser.RedcodeParser;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ferdinand on 25.09.2016.
 */
public class SplTest {
    //simple test to test that a new process is added to the WarriorProcessQueue after a SPL
    //was executed
    @Test
    public void splTest01() {
        String warriorCode = String.join("\n",
                "SPL 2 1",
                "DAT #4 #3",
                "");

        PreGameEngine preGameEngineToTest = new PreGameEngine(3, 1, new Core(3));

        List<InstructionTemplate> listBefore = new RedcodeParser().parse(warriorCode);
        Warrior warriorToTest = new Warrior("Before", new WarriorProcess(), listBefore);
        preGameEngineToTest.setInitialWarriorList(warriorToTest);
        preGameEngineToTest.copyWarriorsToCore();
        GameEngine gameEngineToTest = new GameEngine(100,
                preGameEngineToTest.getInitialWarriorList(), preGameEngineToTest.getPlayground());

        LinkedList<IProcess> processQueue = warriorToTest.getWarriorProcessQueue().getProcessQueue();

        assertEquals(processQueue.size(), 1);
        //execute one step
        gameEngineToTest.gameStep();

        assertEquals(processQueue.size(), 2);
    }
}



