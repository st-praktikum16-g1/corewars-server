package gone.server.execution;

import static gone.server.execution.InstructionPointerCompareAfterOneExecution.compareIpAfterOneExecution;

import org.junit.Test;

/**
 * Created by Ferdinand on 19.08.2016.
 */
public class JmzTest {
    //This Integration-Test is based on our domain-intro
    //https://gitlab.com/noisekick91/Core.War.Intro/blob/master/intro.md

    @Test
    //changes the instruction pointer to where JMZ.A points to
    //if the B-field where JMZ.B points to is == 0
    //here jump
    //validated with CoreWin
    public void testJmz_1() {
        String warriorCode = String.join("\n",
                "JMZ 2 0",
                "DAT #0 #2",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //changes the instruction pointer to where JMZ.A points to
    //if the B-field where JMZ.B points to is == 0
    //here don't jump
    //validated with CoreWin
    public void testJmz_2() {
        String warriorCode = String.join("\n",
                "JMZ 2 1",
                "DAT #0 #1",
                "");
        compareIpAfterOneExecution(3, 1, warriorCode);
    }


    @Test
    //here jump
    //validated with CoreWin
    public void testJmz_3() {
        String warriorCode = String.join("\n",
                "JMZ 2 @1",
                "DAT #0 #0",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //here jump
    //validated with CoreWin
    public void testJmz_4() {
        String warriorCode = String.join("\n",
                "JMZ 2 <1",
                "DAT #0 #1",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }

    @Test
    //here: jump
    // validated with CoreWin
    public void testJmn_3() {
        String warriorCode = String.join("\n",
                "JMZ 2, #0",
                "DAT #0, #0",
                "");
        compareIpAfterOneExecution(3, 2, warriorCode);
    }
}
