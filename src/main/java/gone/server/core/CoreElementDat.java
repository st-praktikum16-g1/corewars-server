package gone.server.core;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.RcArgument;

/**
 * For operations with DAT-instructions.
 */

public class CoreElementDat extends CoreElement {

    /**
     * Constructor for CoreElementDat.
     * @param fieldPrefixA  prefix for the A-field
     * @param argumentA address for the A-field
     * @param fieldPrefixB  prefix for the B-field
     * @param argumentB address for the B-field
     */
    public CoreElementDat(FieldPrefix fieldPrefixA, int argumentA,
                          FieldPrefix fieldPrefixB, int argumentB) {
        this.setAField(new RcArgument());
        this.getAField().setAddress(argumentA);
        this.getAField().setFieldPrefix(fieldPrefixA);

        this.setBField(new RcArgument());
        this.getBField().setAddress(argumentB);
        this.getBField().setFieldPrefix(fieldPrefixB);
    }

    public CoreElementDat(int argumentA, int argumentB) {
        this(FieldPrefix.Direct, argumentA, FieldPrefix.Direct, argumentB);
    }

    @Override
    public CoreElement clone() {
        CoreElementDat result = new CoreElementDat(this.getAField().getFieldPrefix(),
                this.getAField().getAddress(),this.getBField().getFieldPrefix(),
                this.getBField().getAddress());
        return result;
    }

    @Override
    public boolean areEqualFields(CoreElement input) {
        return this.getAField().getFieldPrefix() == input.getAField().getFieldPrefix()
                && this.getAField().getAddress() == input.getAField().getAddress()
                // B
                && this.getBField().getFieldPrefix() == input.getBField().getFieldPrefix()
                && this.getBField().getAddress() == input.getBField().getAddress();
    }
}
