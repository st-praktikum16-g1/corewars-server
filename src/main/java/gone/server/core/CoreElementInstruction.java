package gone.server.core;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.RcArgument;

/**
 * For operations and handling of instructions (ADD, MOV, JMP...).
 */

public class CoreElementInstruction extends CoreElement {
    private IInstruction instruction;

    /* setters & getters */
    public void setInstruction(IInstruction instruction) {
        this.instruction = instruction;
    }

    public IInstruction getInstruction() {
        return instruction;
    }

    @Override
    public CoreElement clone() {
        CoreElementInstruction result = new CoreElementInstruction();
        result.setAField(this.getAField().clone());

        // null pointer exception empty b field
        if (this.getBField() != null) {
            result.setBField(this.getBField().clone());
        } else {
            RcArgument temp = new RcArgument();
            temp.setAddress(0);
            temp.setFieldPrefix(FieldPrefix.Direct);
            result.setBField(temp);
        }

        result.setInstruction(this.getInstruction());
        return result;
    }

    @Override
    public boolean areEqualFields(CoreElement input) {
        if (!(input instanceof CoreElementInstruction)) {
            return false;
        }
        String keyword1 = this.getInstruction().getKeyword();
        String keyword2 = ((CoreElementInstruction) input).getInstruction().getKeyword();
        return areEqualStrings(keyword1, keyword2)
                // A
                && this.getAField().getFieldPrefix() == input.getAField().getFieldPrefix()
                && this.getAField().getAddress() == input.getAField().getAddress()
                // B
                && this.getBField().getFieldPrefix() == input.getBField().getFieldPrefix()
                && this.getBField().getAddress() == input.getBField().getAddress();
    }

}
