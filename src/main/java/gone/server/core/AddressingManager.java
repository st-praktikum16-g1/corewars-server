package gone.server.core;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.RcArgument;

import java.security.InvalidParameterException;

/**
 * Handles the calculation of addresses in the core.
 */
public class AddressingManager {
    /**
     * Calculates the relative address or offset defined by the given operand.
     * @param core  Core the operand and the instruction it comes from is part of
     * @param argument  the operand to calculate the corresponding relative address from
     * @return  relative address defined by given operand
     */
    public int getRelativeAddress(Core core, RcArgument argument) {
        return getRelativeAddress(getIp(core, argument), argument.getIsAField(),
                argument.getAddress(), argument.getFieldPrefix(), core);
    }

    /**
     * Calculates the relative address or offset defined by the given parameters (the parameters
     * usually belong/come from a operand).
     * @param instructionPointer    where the operand comes from
     * @param isAField  true if the operand is a A-field, else false
     * @param addressOnLocalArgument    address from the operand
     * @param prefix    addressing mode from the operand
     * @param core  Core the operand comes from
     * @return  relative address defined by the given data from a operand
     */
    private int getRelativeAddress(int instructionPointer, Boolean isAField, int addressOnLocalArgument,
                                   FieldPrefix prefix, Core core) {
        if (prefix == FieldPrefix.Direct || prefix == FieldPrefix.Immediate) {
            return addressOnLocalArgument;
        }

        CoreElement targetElement = core.getCoreElement(instructionPointer + addressOnLocalArgument);

        //if active, deactivate the predecrement-handling in WarriorProcess -> run()
        /*
        if (prefix == FieldPrefix.IndirectWithPredecrement) {
            targetElement.getBField().setAddress(targetElement.getBField().getAddress() - 1);
        }
        */

        //not in ICWS '88
        /*
        if (prefix == FieldPrefix.IndirectWithPostincrement) {
            targetElement.getBField().setAddress(targetElement.getBField().getAddress() + 1);
        }
        */


        if (isAField) {
            //not in ICWS '88
            /*
            if (prefix == FieldPrefix.IndirectWithPostincrement) {
                return targetElement.getAField().getAddress() - 1 + addressOnLocalArgument;
                //-1 so that the address is used in it's state before incrementation, since it's
                //POST increment
            }
            */
            return targetElement.getBField().getAddress() + addressOnLocalArgument;
        } else {
            //not in ICWS '88
            /*
            if (prefix == FieldPrefix.IndirectWithPostincrement) {
                return targetElement.getBField().getAddress() - 1 + addressOnLocalArgument;
                //-1 so that the address is used in it's state before incrementation
            }
            */
            return targetElement.getBField().getAddress() + addressOnLocalArgument;
        }
    }

    /**
     * Searches for the position of the given operand in the also given core.
     * @param core  core to search in for the given operand
     * @param argument  operand to search for in the core
     * @return  instructionPointer to the instruction that holds the given operand
     * @throws InvalidParameterException    if given operand couldn't be located
     */
    private int getIp(Core core, RcArgument argument) throws InvalidParameterException {
        int index = -1;

        for (int i = 0; i < core.getSize() && index == -1; i++) {
            CoreElement coreElement = core.getCoreElement(i);

            if (coreElement == argument.getCoreElement()) {
                index = i;
            }
        }

        if (index == -1) {
            throw new InvalidParameterException("could not locate the RcArgument in the core");
        }

        return index;
    }

    /**
     * Searches for the given argument's label in the core.
     * @param core  playground to search in
     * @param argument  the operand with the label we want to search for
     * @return  position of the CoreElement in the Core with the searched label
     * @throws InvalidParameterException if the label or the CoreElement belonging to the argument
     *      couldn't be found.
     */
    public int findLabelInCore(Core core, RcArgument argument) throws InvalidParameterException {
        int indexA = -1;//index of the CoreElement the given argument belongs to
        int indexB = -1;//index of CoreElement with the label we are searching for

        for (int i = 0; i < core.getSize() && ( indexA == -1 || indexB == -1 ); i++) {
            CoreElement coreElement = core.getCoreElement(i);

            if (coreElement.hasLabel()) {
                if (coreElement.getLabel().equals(argument.getReferencedLabel())) {
                    indexB = i;
                }
            }
            if (coreElement == argument.getCoreElement()) {
                indexA = i;
            }
        }

        if (indexB == -1) {
            throw new InvalidParameterException("label not found");
        }

        if (indexA == -1) {
            throw new InvalidParameterException("core element of the argument not found");
        }
        return indexB;
    }
}