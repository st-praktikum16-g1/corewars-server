package gone.server.core;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.ICore;
import gone.server.contracts.RcArgument;

import java.util.ArrayList;

/**
 * The Core class holds all core elements in a ring array.
 */
public class Core implements ICore {
    private ArrayList<CoreElement> coreElements;
    private int lastChangedCoreElement = -1;
    private AddressingManager addressingManager = new AddressingManager();

    /**
     * Constructor for the core/playground.
     * @param sizeInput size of the wanted core
     * @throws IllegalArgumentException when sizeInput <0
     */
    public Core(int sizeInput) throws IllegalArgumentException {
        if (sizeInput < 0) {
            throw new IllegalArgumentException("Core.Size must be >= 0");
        }
        coreElements = new ArrayList<>(sizeInput);
        for (int i = 0; i < sizeInput; i++) {
            CoreElementDat dat = new CoreElementDat(0,0);
            dat.getAField().setFieldPrefix(FieldPrefix.Direct);
            dat.getBField().setFieldPrefix(FieldPrefix.Direct);
            coreElements.add(i, dat);
        }
    }

    public Core() {
        coreElements = new ArrayList<>();
    }

    /* setters */

    @Override
    public void setCoreElement(int index, CoreElement coreElementInput) {
        // TODO: check correct index
        lastChangedCoreElement = index;
        coreElements.set(index % coreElements.size(), coreElementInput);
    }

    /* getters */

    public ArrayList<CoreElement> getCoreList() {
        return this.coreElements;
    }

    @Override
    public int getSize() {
        return coreElements.size();
    }

    @Override
    public CoreElement getCoreElement(int index) {
        while (index < 0) {
            index += coreElements.size();
        }
        return coreElements.get(index % coreElements.size());
    }

    @Override
    public int getRelativeAddress(RcArgument argument) {
        return addressingManager.getRelativeAddress(this, argument);
    }

    @Override
    public int getChangedCoreElementIndex() {
        int result = lastChangedCoreElement;
        lastChangedCoreElement = -1;
        return result;
    }

    /**
     * Handles the predecrement if the instruction has a operand with a predecrement addressing mode.
     * @param instructionPointer points to the CoreElement/instruction to work with
     */
    public void executePreAddressingModes(int instructionPointer) {
        CoreElement executingElement = this.getCoreElement(instructionPointer);

        RcArgument fieldA = executingElement.getAField();
        RcArgument fieldB = executingElement.getBField();

        //in ICWS '88 there is only one IndirectWithPredecrement (the '<'), it decrements the
        //B-field of the referenced CoreElement
        if (fieldA.getFieldPrefix() == FieldPrefix.IndirectWithPredecrement) {
            CoreElement referencedElement =
                    this.getCoreElement(instructionPointer + fieldA.getAddress());
            int newAddress = referencedElement.getBField().getAddress() - 1;
            referencedElement.getBField().setAddress(newAddress);
        }
        if (fieldB.getFieldPrefix() == FieldPrefix.IndirectWithPredecrement) {
            CoreElement referencedElement =
                    this.getCoreElement(instructionPointer + fieldB.getAddress());
            int newAddress = referencedElement.getBField().getAddress() - 1;
            referencedElement.getBField().setAddress(newAddress);
        }

    }

    /**
     * Handles the operations needed for instructions with postincrement addressing modes.
     * If the instruction has a operand with a postincrement addressing mode it does the
     * '+1 thing'. Else it does nothing.
     * @param instructionPointer points to the CoreElement/instruction to work with
     */
    //not used in ICWS '88
    /*
    public void executePostAddressingModes(int instructionPointer) {
        CoreElement executingElement = this.getCoreElement(instructionPointer);

        executePostAddressingModesField(executingElement.getAField(), true, instructionPointer);
        executePostAddressingModesField(executingElement.getBField(), false, instructionPointer);
    }
    */

    /**
     * Does the operations connected with a operand with postincrement addressing mode.
     * When the operand is indeed postincrement, it does the '+1 thing'. Does nothing if the operand
     * is not postincrement.
     *
     * @param argument  operand which might have a postincrement addressing mode
     * @param isAField  true if the operand is a A-field, false for B-field
     * @param instructionPointer    points to the instruction/CoreElement the operand belongs to
     */
    //not used in ICWS '88
    /*
    private void executePostAddressingModesField(RcArgument argument, Boolean isAField, int instructionPointer) {
        if (argument.getFieldPrefix() == FieldPrefix.IndirectWithPostincrement) {
            CoreElement referencedElement = this.getCoreElement(instructionPointer + getRelativeAddress(argument));

            if (isAField) {
                if (referencedElement.getAField().hasReferencedLabel()) {
                    throw new IllegalArgumentException();
                }
                referencedElement.getAField().setAddress(referencedElement.getAField().getAddress() + 1);
            } else {
                if (referencedElement.getBField().hasReferencedLabel()) {
                    throw new IllegalArgumentException();
                }
                referencedElement.getBField().setAddress(referencedElement.getBField().getAddress() + 1);
            }
        }
    }
    */

    // only for debugging
    /*
    public void printCore() {
        for (CoreElement temp : this.coreElements) {
            if (temp instanceof CoreElementInstruction) {
                System.out.format("%s %s %s %s %s %s %s %s",
                        temp.getLabel(),
                        ((CoreElementInstruction) temp).getInstruction().getKeyword(),
                        temp.getAField().getFieldPrefix(),
                        temp.getAField().getAddress(),
                        temp.getAField().getReferencedLabel(),
                        temp.getBField().getFieldPrefix(),
                        temp.getBField().getAddress(),
                        temp.getBField().getReferencedLabel());
            } else if (temp instanceof CoreElementDat) {
                System.out.format("%s DAT %s %s %s %s %s %s",
                        temp.getLabel(),
                        temp.getAField().getFieldPrefix(),
                        temp.getAField().getAddress(),
                        temp.getAField().getReferencedLabel(),
                        temp.getBField().getFieldPrefix(),
                        temp.getBField().getAddress(),
                        temp.getBField().getReferencedLabel());
            }
            System.out.println();
        }
    }
    */
}
