package gone.server.parser;

import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstructionLocator;
import gone.server.contracts.IRedcodeParser;
import gone.server.contracts.RcArgument;
import gone.server.gameengine.InstructionLocator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Parses the Redcode input strings to a set of instructions.
 */

public class RedcodeParser implements IRedcodeParser {
    private IInstructionLocator instructionLocator = new InstructionLocator();

    /**
     * Converts a Redcode warrior definition to a parsed set of instructions.
     * @param redcode Warrior as text in Redcode format
     * @return ordered list of instructions
     * @throws IllegalArgumentException if the provided text is not valid Redcode
     */
    public List<InstructionTemplate> parse(String redcode) throws IllegalArgumentException {
        List<InstructionTemplate> result = new LinkedList<>();
        String[] lines = redcode.split("\n");
        int lineIndex = 0;

        for (String line : lines) {
            InstructionTemplate parsedLine;

            try {
                parsedLine = parseLine(line);
            } catch (IllegalArgumentException exception) {
                throw new IllegalArgumentException(String.format(
                        "Parsing error in line %d: %s", lineIndex, exception.getMessage()));
            }

            if (parsedLine != null) {
                result.add(parsedLine);
            }
            lineIndex++;
        }

        return result;
    }

    /**
     * Converts one line of Redcode to one instruction.
     * @param line one line of Redcode
     * @return instruction as template, null means this is a comment
     * @throws IllegalArgumentException if the provided text is not valid Redcode
     */

    private InstructionTemplate parseLine(String line) throws IllegalArgumentException {
        if (isComment(line)) {
            return null;
        }
        InstructionTemplate result = new InstructionTemplate();

        String[] segments = Arrays.stream(line.split(" ")).filter(str -> !str.isEmpty())
                .collect(Collectors.toList()).toArray(new String[0]);

        if (segments.length < 1) {
            throw new IllegalArgumentException("line invalid: too few arguments");
        }

        int parsingIndex = 0;

        String string = getOperationCode(segments[parsingIndex]);
        String stringUppercase = string.toUpperCase();

        // if the fist element in segments it the opcode
        if (instructionLocator.getValidOperationCodes().contains(stringUppercase)) {
            result.setLabel(null);
            result.setHasLabel(false);
            result.setOperationCode(stringUppercase);
        } else {
            //the second element in segments is the opcode the first is a label
            parsingIndex++;
            string = getOperationCode(segments[parsingIndex]);
            stringUppercase = string.toUpperCase();
            if (instructionLocator.getValidOperationCodes().contains(stringUppercase)) {
                result.setLabel(segments[parsingIndex - 1]);
                result.setHasLabel(true);
                result.setOperationCode(string);
            } else {
                throw new IllegalArgumentException("line has no operation code defined");
            }
        }

        parsingIndex++;
        if (segments.length >= parsingIndex + 1) {
            result.setAField(parseArgument(segments[parsingIndex],
                    true));
        }
        if (segments.length >= parsingIndex + 2) {
            result.setBField(parseArgument(segments[parsingIndex + 1],
                    false));
        }

        if (string.equals("DAT")
                && result.getAField().getFieldPrefix() == FieldPrefix.Direct
                && result.getBField().getFieldPrefix() == FieldPrefix.Direct) {
            throw new IllegalArgumentException("invalid DAT with direct arguments");
        }

        return result;
    }

    /**
     * Parses a text argument to a RcArgument definition.
     * @param argument Argument as text with optional mode
     * @return parsed RcArgument
     * @throws IllegalArgumentException when an illegal addressing mode is found
     */
    private RcArgument parseArgument(String argument,
                                     Boolean isAField) throws IllegalArgumentException {
        Map<Character, FieldPrefix> addressingModes
                = getAddressingModes(isAField);
        String trimmedArgument = removeWhiteSpaces(argument);
        Character firstChar = trimmedArgument.charAt(0);
        RcArgument result = new RcArgument();

        result.setFieldPrefix(FieldPrefix.Direct);

        //if firstChar is a addressing mode symbol (#, <, @...)
        if (!(Character.isAlphabetic(firstChar) || Character.isDigit(firstChar)
                || firstChar == '-')) {
            if (addressingModes.containsKey(firstChar)) {
                result.setFieldPrefix(addressingModes.get(firstChar));
            } else {
                throw new IllegalArgumentException("illegal addressing mode");
            }

            trimmedArgument = trimmedArgument.substring(1);
        }

        if (Character.isAlphabetic(trimmedArgument.charAt(0))) {
            // label
            result.setReferencedLabel(trimmedArgument);
        } else {
            result.setAddress(Integer.parseInt(trimmedArgument));
        }
        //TODO parse operands that have a operator (+,-,*,/)


        return result;
    }

    /**
     * checks whether a whole line is a comment.
     * @param redcode line of Redcode
     * @return true, if line is a comment
     */

    private Boolean isComment(String redcode) {
        return removeWhiteSpaces(redcode).startsWith(";");
    }

    /**
     * tries to transform the given text segment to a valid operation code.
     * @param redcode Redcode segment
     * @return 3 letter operation code
     */

    private String getOperationCode(String redcode) {
        if (redcode.length() < 3) {
            throw new IllegalArgumentException("operation code is invalid (to short)");
        }
        return removeWhiteSpaces(redcode);
    }

    /**
     * removes all white spaces and commas in a given string.
     * @param value string with whitespaces
     * @return string without whitespaces
     */

    private String removeWhiteSpaces(String value) {
        String string = value.replaceAll("\\s+","");
        return string.replaceAll(",","");
    }

    /**
     * Gets all addressing mode chars mapped to their ArgumentAddressingMode.
     * @param isAField position A or B to filter the addressing modes
     * @return dictionary of char, ArgumentAddressingMode
     */
    private Map<Character, FieldPrefix> getAddressingModes(Boolean isAField) {
        HashMap<Character, FieldPrefix> result = new HashMap<>();

        result.put('#', FieldPrefix.Immediate);

        /*
        //for ICWS' 94
        if (isAField) {
            result.put('*', FieldPrefix.Indirect);
            result.put('{', FieldPrefix.IndirectWithPredecrement);
            result.put('}', FieldPrefix.IndirectWithPostincrement);
        } else {
            result.put('@', FieldPrefix.Indirect);
            result.put('<', FieldPrefix.IndirectWithPredecrement);
            result.put('>', FieldPrefix.IndirectWithPostincrement);
        }
         */

        //for ICWS' 88
        result.put('@', FieldPrefix.Indirect);
        result.put('<', FieldPrefix.IndirectWithPredecrement);

        return result;
    }
}
