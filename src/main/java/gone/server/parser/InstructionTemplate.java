package gone.server.parser;

import gone.server.contracts.CoreElement;

/**
 * Template for Instructions, contains opcode.
 */

public class InstructionTemplate extends CoreElement {
    private String operationCode;

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    @Override
    public CoreElement clone() {
        return null;
    }

    @Override
    public boolean areEqualFields(CoreElement input) {
        return false;
    }
}
