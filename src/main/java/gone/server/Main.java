package gone.server;

import gone.server.operator.ServerMainModule;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;
import java.util.prefs.InvalidPreferencesFormatException;

/**
 * Main of the server, starts the ServerMainModule.
 */

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException,
            InvalidPreferencesFormatException, IllegalAccessException {
        try {
            String path = System.getProperty("user.home") + File.separator + "gone.corewars"
                    + File.separator + "log" + File.separator + "server" + File.separator + "server.log";

            File file = new File(path);

            if(!file.exists()) {
                file.getParentFile().mkdirs();
            }

            FileHandler fileHandler = new FileHandler(path);
            fileHandler.setFormatter(new SimpleFormatter());

            fileHandler.setLevel(Level.FINEST);

            for (Handler handler : Logger.getGlobal().getHandlers()) {
                Logger.getGlobal().removeHandler(handler);
            }

            Logger.getGlobal().addHandler(fileHandler);

            Logger.getGlobal().log(Level.INFO, "Server starting...");
            ServerMainModule mainModule = new ServerMainModule();

            System.out.print("Press [any key] to exit ...\n\n");

            System.in.read();

            mainModule.teardown();
        } catch (Exception exception) {
            Logger.getGlobal().log(Level.SEVERE, "server main terminated", exception);
        }
    }
}