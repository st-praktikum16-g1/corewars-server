package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * A not immediate -> B-field of CoreElement where SLT-A points to is compared to where SLT-B points
 * to
 * A immediate -> SLT-A compared to where SLT-B points to
 * ...if the first value is less than the second value, the next instruction is skipped
 */
public class SkipIfLessThan implements IInstruction {
    @Override
    public String getKeyword() {
        return "SLT";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);

        RcArgument argumentA = executingElement.getAField();
        int addressA = playground.getRelativeAddress(argumentA);

        RcArgument argumentB = executingElement.getBField();
        int addressB = playground.getRelativeAddress(argumentB);

        if (addressA < addressB) {
            IProcess current = processQueue.getCurrent();
            current.setInstructionPointer(current.getInstructionPointer() + 1);
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getBField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
