package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * The instruction pointer of this process is changed to where the A-field points to, B-field is
 * ignored.
 */
public class JumpInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "JMP";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);
        RcArgument argumentA = executingElement.getAField();
        int addressA = playground.getRelativeAddress(argumentA);

        IProcess currentProcess = processQueue.getCurrent();

        currentProcess.setInstructionPointer(instructionPointer + addressA - 1);
        // -1 because the InstructionPointer is incremented later on again by the run()
        // in WarriorProcess
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getAField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
