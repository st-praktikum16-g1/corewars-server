package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.parser.InstructionTemplate;

/**
 * A is immediate -> A is compared to where the B-field points to.
 * A not immediate -> whole CoreElement where A points to compared with CoreElement of B-field
 * if the compared elements are equal, the next instruction is skipped
 */
public class CompareInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "CMP";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);
        Boolean skipNext;

        RcArgument argumentA = executingElement.getAField();
        int addressA = playground.getRelativeAddress(argumentA);

        RcArgument argumentB = executingElement.getBField();
        int addressB = playground.getRelativeAddress(argumentB);

        if (argumentB.getFieldPrefix() == FieldPrefix.Immediate
                || argumentA.getFieldPrefix() == FieldPrefix.Immediate) {
            // if one operand is immediate: only compare the A and B field
            skipNext = addressA == addressB;
        } else {
            skipNext = playground.getCoreElement(instructionPointer + addressA)
                    .areEqualFields(playground.getCoreElement(instructionPointer + addressB));
        }

        IProcess current = processQueue.getCurrent();
        if (skipNext) {
            current.setInstructionPointer(current.getInstructionPointer() + 1);
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getBField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
