package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * if A immediate -> A is subtracted from the address of B-field
 * if A not immediate -> both A- and B-field are subtracted from the A- respectively B-field where
 * the SUB B-field points to.
 */
public class SubInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "SUB";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();

        CoreElement executingElement = playground.getCoreElement(instructionPointer);
        RcArgument argumentA = executingElement.getAField();
        RcArgument argumentB = executingElement.getBField();
        int addressA = playground.getRelativeAddress(argumentA);
        int addressB = playground.getRelativeAddress(argumentB);

        if (argumentA.getFieldPrefix() == FieldPrefix.Immediate) {
            RcArgument argumentBOfTarget =
                    playground.getCoreElement(instructionPointer + addressB).getBField();
            int oldValue = playground.getRelativeAddress(argumentBOfTarget);
            argumentBOfTarget.setAddress(oldValue - addressA);
        } else {
            CoreElement source = playground.getCoreElement(instructionPointer + addressA);
            CoreElement target = playground.getCoreElement(instructionPointer + addressB);

            int oldAValue = playground.getRelativeAddress(target.getAField());
            int oldBValue = playground.getRelativeAddress(target.getBField());

            target.getAField()
                    .setAddress(oldAValue - playground.getRelativeAddress(source.getAField()));
            target.getBField()
                    .setAddress(oldBValue - playground.getRelativeAddress(source.getBField()));
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getBField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
