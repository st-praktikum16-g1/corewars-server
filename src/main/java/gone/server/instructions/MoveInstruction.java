package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * A immediate -> A-field is copied to where B-field points to.
 * A not immediate -> complete CoreElement to where A-field points to is copied to where B point to
 */
public class MoveInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "MOV";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);

        RcArgument argumentA = executingElement.getAField();
        RcArgument argumentB = executingElement.getBField();

        if (argumentB.getFieldPrefix() == FieldPrefix.Immediate) {
            throw new IllegalArgumentException("B Argument");
        }

        int addressA = playground.getRelativeAddress(argumentA);
        int addressB = playground.getRelativeAddress(argumentB);

        if (argumentA.getFieldPrefix() == FieldPrefix.Immediate) {
            playground.getCoreElement(instructionPointer + addressB).getBField().setAddress(argumentA.getAddress());
        } else {
            CoreElement toMove = playground.getCoreElement(instructionPointer + addressA);
            playground.setCoreElement(instructionPointer + addressB, toMove.clone());
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getBField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
