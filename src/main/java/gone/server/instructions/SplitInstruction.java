package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.InstructionTemplate;

/**
 * Instruction splits the execution and creates a new process with the instruction pointer to where.
 * SPL A points to, SPL B is ignored
 */
public class SplitInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "SPL";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);
        RcArgument argumentA = executingElement.getAField();
        int addressA = playground.getRelativeAddress(argumentA);

        WarriorProcess splitProcess = new WarriorProcess();
        splitProcess.setInstructionPointer(instructionPointer + addressA);
        //TODO really setIP(instructionPointer + addressA) or just setIP(addressA)?

        processQueue.add(splitProcess);
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getAField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
