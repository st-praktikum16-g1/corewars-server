package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * If where the B-field points to is != 0, instruction pointer is set to were the A-field points to.
 */
public class JumpIfNotZeroInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "JMN";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();
        CoreElement executingElement = playground.getCoreElement(instructionPointer);

        int valueOfBField;
        if (executingElement.getBField().getFieldPrefix() != FieldPrefix.Immediate) {
            int relativeAddress = playground.getRelativeAddress(executingElement.getBField());
            valueOfBField = playground.getCoreElement(instructionPointer + relativeAddress).getBField().getAddress();
        } else {
            valueOfBField = executingElement.getBField().getAddress();
        }

        if (valueOfBField != 0) {
            IProcess current = processQueue.getCurrent();

            int valueOfAField = playground.getRelativeAddress(executingElement.getAField());
            current.setInstructionPointer(instructionPointer + valueOfAField - 1);
            // Jump, -1 because the InstructionPointer is incremented later on again by the run()
            // in WarriorProcess
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getAField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
