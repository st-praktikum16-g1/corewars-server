package gone.server.instructions;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.contracts.RcArgument;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * this instruction decrements the value of the B field and executes a jump,
 * if it does not reach zero
 * B immediate -> B is decremented by 1, if its then != 0 the instructionPointer of this process is
 * changed to where the A-field points to, so Process execution jumps to where A points to.
 * B not immediate -> field where the B-field points to gets -=1, ...
 */
public class DecrementAndJumpIfNotZeroInstruction implements IInstruction {
    @Override
    public String getKeyword() {
        return "DJN";
    }

    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        int instructionPointer = processQueue.getCurrent().getInstructionPointer();

        CoreElement executingElement = playground.getCoreElement(instructionPointer);
        RcArgument argumentB = executingElement.getBField();

        int addressB = playground.getRelativeAddress(argumentB);
        CoreElement target = playground.getCoreElement(instructionPointer + addressB);
        int valueOfBField = target.getBField().getAddress();
        target.getBField().setAddress(valueOfBField - 1); // Decrement

        if (valueOfBField != 1) { // or valueOfBField -1 != 0
            IProcess current = processQueue.getCurrent();

            int valueOfAField = playground.getRelativeAddress(executingElement.getAField());
            current.setInstructionPointer(instructionPointer + valueOfAField - 1);
            // Jump, -1 because the InstructionPointer is incremented later on again by the run()
            // in WarriorProcess
        }
    }

    @Override
    public boolean checkIfSyntaxCorrect(InstructionTemplate toCheck) {
        return toCheck.getAField().getFieldPrefix() != FieldPrefix.Immediate;
    }
}
