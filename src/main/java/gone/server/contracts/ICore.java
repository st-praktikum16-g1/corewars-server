package gone.server.contracts;

/**
 * The core contains all CoreElements in a ring-array.
 */
public interface ICore {
    /**
     * Sets the given CoreElement at the given index in the core.
     * @param index position in the Core where the CoreElement will be set
     * @param coreElementInput  the CoreElement that will be stored in the core
     */
    void setCoreElement(int index, CoreElement coreElementInput);

    /**
     * Returns the size (amount of CoreElements it can hold).
     * @return  size of the core
     */
    int getSize();

    /**
     * Returns the CoreElement at the given index.
     * Also works with index bigger than CoreSize by using a modulo operation.
     *
     * @param index position in the core
     * @return  CoreElement at the given position
     */
    CoreElement getCoreElement(int index);

    /**
     * Calculates the offset relative from the argument where the given RcArgument eventually points
     * to.
     * Works with all addressing modes (immediate, direct, indirect, with predecrement and
     * postincrement)
     *
     * @param argument  the operand (A- or B-field)
     * @return address offset where the given operand points to
     */
    int getRelativeAddress(RcArgument argument);

    int getChangedCoreElementIndex();
}
