package gone.server.contracts;

/**
 * Signals how the data or address of this operand has to be interpreted.
 * Immediate: # not really an address, the data is immediate available for use by the instruction
 * Direct: no symbol/$ address is used as offset from address where it was fetched
 * Indirect: * to A-field or @ to B-field
 *      first the value of address is used as offset from where it was fetched, than the A- or
 *      B-field there is used as offset for the original calling address to get its data from there
 * IndirectWithPredecrement: { to A-field or < to B field
 *      like normal indirect but where the * or @ points to, is first decremented by one (-1)
 *      before it is used further
 * IndirectWithPostincrement: } to A-field or > to B field
 *      like with IndirectWithPredecrement but with a +1 and after it was used
 * In ICWS '88 only Immediate with #, Direct without symbol, Indirect with @ and
 * IndirectWithPredecrement with < are allowed.
 */

public enum FieldPrefix {
    Immediate,
    Direct,
    Indirect,
    IndirectWithPredecrement,
    IndirectWithPostincrement
}
