package gone.server.contracts;

import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

/**
 * Instructions define what happens when they are executed.
 */

public interface IInstruction  {
    String getKeyword();

    /**
     * Defines the execution of the instruction.
     * @param playground    core on which the execution will take place
     * @param processQueue  processQueue of the warrior the instruction is part of
     */
    void run(Core playground, IProcessQueue processQueue);

    /**
     * Checks if the Syntax of the instruction is valid.
     * @param toCheck   instruction to check
     * @return  true if the instruction is valid, else false
     */
    boolean checkIfSyntaxCorrect(InstructionTemplate toCheck);
}
