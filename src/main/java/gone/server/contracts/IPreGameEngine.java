package gone.server.contracts;

/**
 * Is used to do preparations for the gameEngine like loading a warrior in the core or set a start
 * player.
 */
public interface IPreGameEngine {
}
