package gone.server.contracts;

import java.util.List;

/**
 * Contains the opcodes of all available instruction.
 */
public interface IInstructionLocator {
    IInstruction getInstructionByOpCode(String opCode);

    List<String> getValidOperationCodes();
}