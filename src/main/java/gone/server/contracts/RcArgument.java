package gone.server.contracts;

/**
 * Is a operand of a instruction. It is either a A- or B-field, has a address and a addressing mode.
 */

public class RcArgument {
    CoreElement coreElement;
    FieldPrefix fieldPrefix;
    String referencedLabel;
    int address;
    Boolean isAField;

    /* setters */
    public void setCoreElement(CoreElement value) {
        this.coreElement = value;
    }

    public void setFieldPrefix(FieldPrefix value) {
        this.fieldPrefix = value;
    }

    public void setReferencedLabel(String getReferencedLabel) {
        this.referencedLabel = getReferencedLabel;
        this.address = 0;
    }

    public void setAddress(int address) {
        this.address = address;
        this.referencedLabel = null;
    }

    /* getters */
    public CoreElement getCoreElement() {
        return coreElement;
    }

    public FieldPrefix getFieldPrefix() {
        return fieldPrefix;
    }

    public String getReferencedLabel() {
        return referencedLabel;
    }

    public int getAddress() {
        return address;
    }

    public boolean getIsAField() {
        return isAField;
    }

    public void setIsAField(Boolean value) {
        isAField = value;
    }

    /* class methods */

    public Boolean hasReferencedLabel() {
        return referencedLabel != null;
    }

    @Override
    public RcArgument clone() {
        RcArgument result = new RcArgument();

        result.setAddress(this.getAddress());
        result.setFieldPrefix(this.getFieldPrefix());

        return result;
    }
}
