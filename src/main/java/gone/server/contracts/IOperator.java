package gone.server.contracts;

/**
 * The operator controls the creation and execution of the game, how player can connect and the
 * network.
 */
public interface IOperator {
}
