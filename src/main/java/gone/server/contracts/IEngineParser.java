package gone.server.contracts;

import gone.server.gameengine.Warrior;
import gone.server.parser.InstructionTemplate;

import java.util.List;

/**
 * Not used.
 */

public interface IEngineParser {
    List<InstructionTemplate> getInstructionTemplatesByWarrior(Warrior warriorName);
}

