package gone.server.contracts;

import gone.server.core.Core;

/**
 * Belongs to a warrior, contains information like the InstructionPointer.
 */
public interface IProcess {
    void run(Core playground, IProcessQueue processQueue);

    void setInstructionPointer(int instructionPointer);

    int getInstructionPointer();
}
