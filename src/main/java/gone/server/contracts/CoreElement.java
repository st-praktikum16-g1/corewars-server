package gone.server.contracts;

import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;

/**
 * Is one cell in the core, it consists of a A-field, B-field and may have a label.
 */
public abstract class CoreElement {
    private String label;
    private boolean hasLabel;
    private RcArgument argumentA;
    private RcArgument argumentB;

    /* setters */

    public void setHasLabel(boolean value) {
        this.hasLabel = value;
    }

    /**
     * Sets the Label of this CoreElement.
     * @param label label to be set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Sets the A-field of this CoreElement.
     * @param argumentA operand to be set as A-field of this CoreElement
     */
    public void setAField(RcArgument argumentA) {
        this.argumentA = argumentA;
        this.argumentA.setCoreElement(this);
        this.argumentA.setIsAField(true);
    }

    /**
     * Sets the B-field of this CoreElement.
     * @param argumentB operand to be set as B-field of this CoreElement
     */
    public void setBField(RcArgument argumentB) {
        this.argumentB = argumentB;
        this.argumentB.setCoreElement(this);
        this.argumentB.setIsAField(false);
    }

    /* getters */
    public boolean hasLabel() {
        return hasLabel;
    }

    public String getLabel() {
        return label;
    }

    public RcArgument getAField() {
        return argumentA;
    }

    public RcArgument getBField() {
        return argumentB;
    }

    /* class methods */

    /**
     * Creates a exact copy of this CoreElement.
     * @return  the new copy
     */
    @Override
    public abstract CoreElement clone();

    /**
     * Compares this CoreElement with the one given.
     * Compares all fields of the two CoreElements, (opcode, A-field, B-field, prefix, address)
     * @param input the other CoreElement to compare with
     * @return  true if all fields have the same values, else false
     */
    public abstract boolean areEqualFields(CoreElement input);

    /**
     * Compares if the two given strings are equal.
     * @param string1   first string for comparison
     * @param string2   second string for comparison
     * @return  true if strings are equal
     */
    protected boolean areEqualStrings(Object string1, Object string2) {
        if ((string1 instanceof String) && (string2 instanceof String)) {
            return string1.equals(string2);
        } else {
            return string1 == string2;
        }
    }
}
