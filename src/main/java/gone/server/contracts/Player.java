package gone.server.contracts;

import java.net.InetAddress;
import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Someone connected to the server.
 */
public class Player {
    private String name;
    private String sessionId;
    private String redcode;
    private boolean connected;
    private Duration connectedTimer;
    private int playedGames;
    private int waitedGames;
    private boolean ready;

    /**
     * Constructor for player.
     * @param name player name
     * @param sessionId session ID of the socket used for this client
     * @param connected true if connected
     * @param playedGames   number of played games
     * @param waitedGames   number of waited games
     * @param ready true if player is ready
     */
    public Player(String name, String sessionId, boolean connected,
                  int playedGames, int waitedGames, boolean ready) {
        setName(name);
        setSessionId(sessionId);
        setConnected(connected);
        setConnectedTimer(Duration.ZERO);
        setPlayedGames(playedGames);
        setWaitedGames(waitedGames);
        setReady(ready);
        Logger.getGlobal().log(Level.INFO,"Name: " + name + "SessionID: " + sessionId
                + "Connected: " + connected + "PlayedGames: " + "WaitedGames: " + waitedGames
                + "Ready: " + ready);
    }

    /* setters */

    // TODO setters: value check

    public void setName(String value) {
        this.name = value;
    }

    public void setSessionId(String value) {
        this.sessionId = value;
    }

    public void setConnected(boolean value) {
        this.connected = value;
    }

    /**
     * Setter for ConnectedTimer.
     * @param value time the player is already connected
     * @throws IllegalArgumentException when value is <0
     */
    public void setConnectedTimer(Duration value) throws IllegalArgumentException {
        if (value.isNegative()) {
            throw new IllegalArgumentException("duration must be >= 0");
        }
        this.connectedTimer = value;
    }

    /**
     * Setter for amount of games the player played in this session.
     * @param value amount of games the player played
     * @throws IllegalArgumentException when value is <0
     */
    public void setPlayedGames(int value) throws IllegalArgumentException {
        if (value < 0) {
            throw new IllegalArgumentException("set played games must be >= 0");
        }
        this.playedGames = value;
    }

    /**
     * Setter for the amount of games the player already waited.
     * @param value amount of games the player waited
     * @throws IllegalArgumentException when value <0
     */
    public void setWaitedGames(int value) throws IllegalArgumentException {
        if (value < 0) {
            throw new IllegalArgumentException("set waited games must be >= 0");
        }
        this.waitedGames = value;
    }

    public void setReady(boolean value) {
        this.ready = value;
    }

    public void setRedcode(String redcode) {
        this.redcode = redcode;
    }

    /* getters */

    public String getName() {
        return name;
    }

    public String getRedcode() {
        return redcode;
    }

    public String getSessionId() {
        return sessionId;
    }

    public boolean isConnected() {
        return connected;
    }

    public Duration getConnectedTimer() {
        return connectedTimer;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public int getWaitedGames() {
        return waitedGames;
    }

    public boolean isReady() {
        return ready;
    }
}
