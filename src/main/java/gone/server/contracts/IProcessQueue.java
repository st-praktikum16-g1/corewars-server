package gone.server.contracts;

import gone.server.core.Core;

/**
 * A queue of processes, controls the order they are executed. Belongs to a warrior.
 */
public interface IProcessQueue {
    void run(Core playground);

    void add(IProcess toAdd);

    void remove(IProcess toKill);

    IProcess getCurrent();
}
