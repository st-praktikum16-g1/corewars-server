package gone.server.contracts;

import gone.server.parser.InstructionTemplate;

/**
 * Not used.
 */

public interface IEngineCore {
    CoreElement getCoreElementByAddress(int address);
    
    void setCoreElementByAddress(InstructionTemplate instructionTemplate, int address);
}
