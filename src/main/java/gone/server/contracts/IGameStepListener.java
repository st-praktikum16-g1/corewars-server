package gone.server.contracts;

/**
 * Watches over execution steps.
 */
public interface IGameStepListener {
    void gameStep(int changedOpCodeIndex, String newOpCode, int oldIp, int newIp,
                  boolean firstPlayerActive);

    void gameResult(String playerName);
}
