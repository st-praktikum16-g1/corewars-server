package gone.server.common;

/**
 * Contains the possible Redcode-standard.
 * Created by matthias on 22.06.16.
 */
public enum RcStandard {
    ICWS88,
    ICWS94
}
