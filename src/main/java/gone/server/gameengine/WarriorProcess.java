package gone.server.gameengine;

import gone.server.contracts.CoreElement;
import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.core.Core;
import gone.server.core.CoreElementInstruction;

/**
 * Holds the instruction pointer and increments it after an execution.
 */

public class WarriorProcess implements IProcess {
    private int instructionPointer;
    private int processId;

    /* constructor */


    public WarriorProcess() {
        setInstructionPointer(0);
    }

    /* setters */

    @Override
    public void setInstructionPointer(int value) {
        // TODO: exception correct value
        this.instructionPointer = value;
    }

    public void setProcessId(int processIdInput) {
        // TODO: exception correct value
        this.processId = processIdInput;
    }

    /* getters */

    @Override
    public int getInstructionPointer() {
        return instructionPointer;
    }

    public int getProcessId() {
        return processId;
    }

    /* class methods */

    /**
     * Executes the CoreElement where the instructionPointer points to.
     * If the instructionPointer points to a CoreElementInstruction, this instruction will
     * be executed and the instructionPointer incremented.
     *
     * <p>Else if the instructionPointer points to a CoreElementDat, the current process gets removed
     * from the processQueue.
     *
     * @param playground    the core where the execution takes place
     * @param processQueue the warrior processes in correct order
     */
    @Override
    public void run(Core playground, IProcessQueue processQueue) {
        CoreElement toExecute = playground.getCoreElement(instructionPointer);
        if (toExecute instanceof CoreElementInstruction) {
            CoreElementInstruction instruction = (CoreElementInstruction) toExecute;


            playground.executePreAddressingModes(instructionPointer);//if active, deactivate
            //predecrement-handling in the AddressingManager -> getRelativeAddress()
            instruction.getInstruction().run(playground, processQueue);
            //playground.executePreAddressingModes(instructionPointer);
            //postincrement is not part of ICWS '88
            ++instructionPointer;
        } else {
            processQueue.remove(this);
        }
    }
}