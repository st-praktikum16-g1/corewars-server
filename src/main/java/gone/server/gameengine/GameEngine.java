package gone.server.gameengine;

import gone.server.contracts.IGameEngine;
import gone.server.core.Core;

import java.util.List;

/**
 * Handles the execution on the level of the warriors, stops the game if needed.
 */

public class GameEngine implements IGameEngine {
    private static int MAX_CYCLES_COUNT;
    private static int ACTUAL_PLAYERS_COUNT;
    private GameInfo gameInfo;
    private WarriorRunQueue warriorRunQueue;
    private Core playCore;

    /**
     * Constructor for GameEngine.
     *
     * @param   maxCyclesCount  the limit of how many Cycles will be executed
     * @param   preGeWarriorList    the warriors coming from the PreGameEngine
     * @param   coreVarFromPge  the core to use at playground
     */

    public GameEngine(int maxCyclesCount, List<Warrior> preGeWarriorList, Core coreVarFromPge)
            throws IllegalArgumentException {
        setMaxCyclesCount(maxCyclesCount);
        setWarriorRunQueue(preGeWarriorList);
        setPlayCore(coreVarFromPge);
        setActualPlayersCount(preGeWarriorList.size());
        setGameInfo(0, true, false);
    }

    /* setters */

    private void setGameInfo(int roundNumber, boolean running, boolean isDraw) {
        this.gameInfo = new GameInfo(roundNumber, running, isDraw);
    }

    private static void setActualPlayersCount(int value) {
        ACTUAL_PLAYERS_COUNT = value;
    }

    /**
     * Creates a new WarriorRunQueue.
     * @param value list of warriors that will be loaded in the new WarriorRunQueue
     * @throws IllegalArgumentException when the warrior list is empty
     */
    public void setWarriorRunQueue(List<Warrior> value) throws IllegalArgumentException {
        this.warriorRunQueue = new WarriorRunQueue(value);
    }

    /**
     *Setter for the playground/core to use.
     * @param value core to set
     * @throws IllegalArgumentException if the given core is empty
     */
    private void setPlayCore(Core value) throws IllegalArgumentException {
        if (value.getSize() == 0) {
            throw new IllegalArgumentException("Core is empty");
        }
        this.playCore = value;
    }

    /**
     * Setter for how many cycles will at maximum executed. Sets the MAX_CYCLES_COUNT.
     * @param value maximum cycles to be executed
     * @throws IllegalArgumentException if the given value is <0
     */
    private static void setMaxCyclesCount(int value) throws IllegalArgumentException {
        if (value <= 0) {
            throw new IllegalArgumentException("MAX_CYCLES_COUNT setter value is <= 0");
        }
        MAX_CYCLES_COUNT = value;
    }

    /* getters */

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public static int getActualPlayersCount() {
        return ACTUAL_PLAYERS_COUNT;
    }

    public WarriorRunQueue getWarriorRunQueue() {
        return warriorRunQueue;
    }

    public Core getPlayCore() {
        return playCore;
    }

    public static int getMaxCyclesCount() {
        return MAX_CYCLES_COUNT;
    }

    /* class methods */

    public boolean areAllWarriorsAlive() {
        return warriorRunQueue.getWarriorRunQueue().size() >= getActualPlayersCount();
    }

    /**
     * execute just one single step of the game, return int roundNumber.
     * if one warrior dies (or GameEngine reached MAX_CYCLES_COUNT) don't execute any more gameSteps
     */
    public GameInfo gameStep() {

        if (areAllWarriorsAlive()) {

            Warrior warriorToExecute = warriorRunQueue.getWarriorRunQueue().element();
            warriorRunQueue.run(this.playCore);
            gameInfo.roundNumber++;

            if (!(warriorToExecute.getIsAlive())) {
                this.warriorRunQueue.getWarriorRunQueue().remove(warriorToExecute);

                if (!areAllWarriorsAlive()) {
                    gameInfo.setGameRunning(false);
                    return this.getGameInfo();
                }
            }
            if (gameInfo.roundNumber >= MAX_CYCLES_COUNT) {
                gameInfo.setGameRunning(false);
                return this.getGameInfo();
            }
        }
        return this.getGameInfo();
    }
}
