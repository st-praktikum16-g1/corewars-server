package gone.server.gameengine;

import gone.server.contracts.IProcess;
import gone.server.core.Core;
import gone.server.parser.InstructionTemplate;

import java.util.List;

/**
 * Data structure for the warrior to gather information used for warrior queue etc.
 */


public class Warrior {
    private boolean isAlive;
    private int startAddressAtCore; // startAddress + length of InstrList = area of warrior code
    private String name;
    private boolean isStartPlayer;
    private WarriorProcessRunQueue warriorProcessQueue; // each warrior has its own processQ
    private List<InstructionTemplate> rcProgram;

    /* constructor */

    /**
     * Constructor for Warrior.
     * @param nameInput warrior name
     * @param warriorProcess    process for the warrior
     * @param insList   list of the warrior's instructions
     */
    public Warrior(String nameInput, IProcess warriorProcess, List<InstructionTemplate> insList) {
        setWarriorProcessQueue(warriorProcess);
        setIsAlive(true); // after initialization warrior sure is alive
        setStartAddressAtCore(0); // need to update the startAddress in the preGameEngine
        setName(nameInput);
        setStartPlayer(false); // gets updated in PreGameEngine
        setRcProgram(insList);
    }

    /* setters */

    /**
     * Sets the Redcode program for this warrior.
     * @param value redcode programm to set
     * @throws IllegalArgumentException if the given list is empty
     */
    private void setRcProgram(List<InstructionTemplate> value) throws IllegalArgumentException {
        if (value.size() == 0) {
            throw new IllegalArgumentException("empty instruction template list!");
        }
        this.rcProgram = value;
    }

    /**
     * Sets the given process as new WarriorProcessRunQueue.
     * @param value process to set
     */
    private void setWarriorProcessQueue(IProcess value) {
        this.warriorProcessQueue = new WarriorProcessRunQueue(value);
    }


    /**
     * Sets the instructionPointer for the first process in the ProcessQueue for this warrior.
     * @param value value for this instructionPointer for the first process in the ProcessQueue
     */
    private void setFirstProcessInstructionPtr(int value) {
        this.getWarriorProcessQueue().getProcessQueue().getFirst().setInstructionPointer(value);
    }

    /**
     * sets the name for this warrior.
     * @param nameInput name for this warrior
     * @throws IllegalArgumentException if the input is empty
     */
    private void setName(String nameInput) throws IllegalArgumentException {
        if (nameInput.isEmpty()) {
            throw new IllegalArgumentException("name is not allowed to be empty!");
        }
        this.name = nameInput;
    }

    /**
     * Sets the position of the first instruction of this warrior in the core. Also sets the
     * instructionPointer for the first process in the ProcessQueue there.
     * @param startAddressAtCore    position of the first instruction of this warrio in the core
     * @throws IllegalArgumentException  id the given startAddressAtCore is <0
     */
    public void setStartAddressAtCore(int startAddressAtCore) throws IllegalArgumentException {
        if (startAddressAtCore < 0) {
            throw new IllegalArgumentException("start address < 0");
        }
        this.startAddressAtCore = startAddressAtCore;
        this.setFirstProcessInstructionPtr(startAddressAtCore);
    }

    /**
     * Sets if this warrior is still alive, so if this warriorProcessQueue is empty.
     * @param value true if this warrior is still alive
     */
    private void setIsAlive(boolean value) {
        isAlive = value;
    }

    /**
     * Sets if the player this warrior belongs to is the player that will have the first execution
     * in this game.
     * @param startPlayer true if this warrior will start
     */
    public void setStartPlayer(boolean startPlayer) {
        isStartPlayer = startPlayer;
    }

    /**
     * Executes the next process in this warriorProcessQueue, if this queue is not empty.
     * If after the execution this warriorProcessQueue is empty, it sets this warrior to
     * isAlive = false
     * @param core  playground for the execution
     */
    public void run(Core core) {
        // only execute if warriorProcessQ is not empty
        if (this.getWarriorProcessQueue().getProcessQueue().size() > 0) {
            this.warriorProcessQueue.run(core);
        }
        // after execution warriorProcessQ now is maybe empty
        if (this.getWarriorProcessQueue().getProcessQueue().size() == 0) {
            this.setIsAlive(false);
        }
    }

    /* getters */

    public String getName() {
        return name;
    }

    public int getStartAddressAtCore() {
        return startAddressAtCore;
    }

    public boolean getIsAlive() {
        return isAlive;
    }

    public boolean isStartPlayer() {
        return isStartPlayer;
    }

    public WarriorProcessRunQueue getWarriorProcessQueue() {
        return warriorProcessQueue;
    }

    public List<InstructionTemplate> getRcProgram() {
        return rcProgram;
    }

    public int getInstructionsCount() {
        return rcProgram.size();
    }
}
