package gone.server.gameengine;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IInstruction;
import gone.server.contracts.IInstructionLocator;
import gone.server.contracts.IPreGameEngine;
import gone.server.contracts.RcArgument;
import gone.server.core.AddressingManager;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.parser.InstructionTemplate;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


/**
 * Prepares the core and warrior list for the GameEngine.
 */

public class PreGameEngine implements IPreGameEngine {
    private static int MAX_START_INSTRUCTIONS_COUNT;//max size for a warrior to be copied to the
    //core, warrior can grow lager later on.
    private static int MAX_PLAYER_COUNT;
    private List<Warrior> initialWarriorList;
    private Core playground;
    private IInstructionLocator instructionLocator = new InstructionLocator();
    private static final Random RANDOM = new Random();

    /**
     * Constructor for the PreGameEngine.
     * @param maxStartInstructionsCount max size allowed for a warrior to be copied to the core
     * @param maxPlayerCount    the maximal amount of plays that can compete on one core
     * @param playground    the core to play on
     */
    public PreGameEngine(int maxStartInstructionsCount, int maxPlayerCount, Core playground) {
        this.setPlayground(playground);
        this.setMaxPlayerCount(maxPlayerCount);
        this.setMaxStartInstructionsCount(maxStartInstructionsCount);
        this.initialWarriorList = new ArrayList<>(MAX_PLAYER_COUNT);
    }

    /* setters */

    /**
     * Setter for the core.
     * @param core the core to set
     * @throws IllegalArgumentException if core is empty
     */
    public void setPlayground(Core core) throws IllegalArgumentException {
        if (core.getSize() == 0) {
            throw new IllegalArgumentException("Core is empty");
        }
        this.playground = core;
    }

    /**
     * Sets the maximal allow amount of players playing on the core. Can either by 1 or 2.
     * @param value max number of player alowed on the core, either 1 or 2
     * @throws IllegalArgumentException when the given value is not 1 or 2
     */
    public void setMaxPlayerCount(int value) throws IllegalArgumentException {
        if (!(value == 1 || value == 2)) {
            throw new IllegalArgumentException("MAX_PLAYER_COUNT must be 1 or 2 !");
        }
        MAX_PLAYER_COUNT = value;
    }

    /**
     * Sets the maximum allowed number of instruction of a warrior to be copied to the core.
     * @param value limit for amount of instructions for a warrior
     * @throws IllegalArgumentException when value is <0 or coreSize / MAX_PLAYER_COUNT
     */
    public void setMaxStartInstructionsCount(int value)
            throws IllegalArgumentException {
        if (value <= 0) {
            throw new IllegalArgumentException("MAX_START_INSTRUCTIONS_COUNT must be > 0 !");
        } else if (value > (this.getPlayground().getSize() / getMaxPlayerCount())) {
            throw new IllegalArgumentException("MAX_START_INSTRUCTIONS_COUNT must be <= "
            + "Core.Size / MAX_PLAYER_COUNT");
        }
        MAX_START_INSTRUCTIONS_COUNT = value;
    }

    /**
     * check if the parameter warrior.InstructionCount is inbound and if yes
     * add those 2 warriors to the initial warrior list of the PGE
     * @param warriorOne    a first warrior
     * @param warriorTwo    a second warrior
     * @throws IllegalArgumentException invalid input format
     */
    public void setInitialWarriorList(Warrior warriorOne, Warrior warriorTwo)
                throws IllegalArgumentException {
        List<Warrior> tempWarriorList = new LinkedList<>();
        tempWarriorList.add(warriorOne);
        tempWarriorList.add(warriorTwo);

        if (warriorOne.getName().equals(warriorTwo.getName())) {
            throw new IllegalArgumentException("warriors are not allowed to have the same name!");
        }

        for (int i = 0; i < tempWarriorList.size(); i++) {
            if (tempWarriorList.get(i).getInstructionsCount()
                    > (playground.getSize() / MAX_PLAYER_COUNT)) {

                throw new IllegalArgumentException("InstructionCount of Warrior "
                        + i + " > Core.Size() / MAX_PLAYER_COUNT: "
                        + tempWarriorList.get(i).getInstructionsCount()
                        + " > " + playground.getSize() / MAX_PLAYER_COUNT);
            }
            if (tempWarriorList.get(i).getInstructionsCount() > MAX_START_INSTRUCTIONS_COUNT) {

                throw new IllegalArgumentException("InstructionCount of Warrior "
                        + i + " > MAX_START_INSTRUCTIONS_COUNT: "
                        + tempWarriorList.get(i).getInstructionsCount()
                        + " > " + MAX_START_INSTRUCTIONS_COUNT);
            }
        }
        this.initialWarriorList.add(warriorOne);
        this.initialWarriorList.add(warriorTwo);
    }

    /**
     * Sets warrior list for only 1 player.
     * @param warriorOne the warrior that will be used
     * @throws IllegalArgumentException warrior is to large
     */
    public void setInitialWarriorList(Warrior warriorOne) throws IllegalArgumentException {

        if (warriorOne.getInstructionsCount() > playground.getSize()) {
            throw new IllegalArgumentException("InstructionCount of Warrior1 "
                    + " > Core.Size() : " + warriorOne.getInstructionsCount()
                    + " > " + playground.getSize());
        }
        if (warriorOne.getInstructionsCount() > MAX_START_INSTRUCTIONS_COUNT) {
            throw new IllegalArgumentException("InstructionCount of Warrior1 "
                    + " > MAX_START_INSTRUCTIONS_COUNT: " + warriorOne.getInstructionsCount()
                    + " > " + MAX_START_INSTRUCTIONS_COUNT);
        }
        this.initialWarriorList.add(warriorOne);
    }

    /* getters */

    public Core getPlayground() {
        return playground;
    }

    public static int getMaxPlayerCount() {
        return MAX_PLAYER_COUNT;
    }

    public List<Warrior> getInitialWarriorList() {
        return initialWarriorList;
    }

    public static int getMaxStartInstructionsCount() {
        return MAX_START_INSTRUCTIONS_COUNT;
    }

    /* class methods */

    public int getStartPositionByWarrior(Warrior warrior) {
        return warrior.getStartAddressAtCore();
    }

    /**
     * Fills every field in the core with DAT 0 0 (use core constructor).
     * @param sizeInput the size of the core
     * @return a Core variable filled with DAT 0 0
     */
    public Core initiateCore(int sizeInput) {
        return new Core(sizeInput);
    }

    /**
     * Randomly determine which warrior will start.
     * @return random int 0 or 1
     */
    public int identifyStartPlayer() {
        return RANDOM.nextInt(2);
    }

    /**
     * Calculate where to start to copy the startPlayer-warriorCode to the core
     * use random influence, restriction: core should not be empty.
     * @return a random int index in the bounds of the core
     * @throws IllegalArgumentException if core is empty
     */
    public int startPlayerFirstInstructionAtCore() throws IllegalArgumentException {
        if (playground.getSize() <= 0) {
            throw new IllegalArgumentException("Core is empty");
        }
        return RANDOM.nextInt(playground.getSize());
    }

    /**
     * Works similar to the startPlayerFirstInstructionAtCore, but with the restriction,
     * that both players code must not overlap.
     * @param startPlayerIndex  contains the information which player is the one to start the game
     * @param secondPlayerIndex information which player is the second one (is 0 or 1)
     * @return a random int index in the bounds of the core, so the 2 warriors code don't overlap!
     */
    public int secondPlayerFirstInstructionAtCore(int startPlayerIndex, int secondPlayerIndex)
            throws IllegalArgumentException {
        if (playground.getSize() == 0) {
            throw new IllegalArgumentException("Core is empty");
        } else if (startPlayerIndex == secondPlayerIndex) {
            throw new IllegalArgumentException("1st/2nd player index at warriorList are equal");
        }
        Warrior startPlayer = initialWarriorList.get(startPlayerIndex);
        Warrior secondPlayer = initialWarriorList.get(secondPlayerIndex);

        int freeCoreElements = playground.getSize() - startPlayer.getInstructionsCount();
        int maxOffset = freeCoreElements - secondPlayer.getInstructionsCount();
        if (maxOffset < 0) {
            throw new IllegalArgumentException("no free coreElements left to copy the 2nd player");
        }
        maxOffset = RANDOM.nextInt(maxOffset + 1);
        int startPlayerLastIndex =
                startPlayer.getStartAddressAtCore() + startPlayer.getInstructionsCount() - 1;
        return (1 + startPlayerLastIndex + maxOffset) % this.getPlayground().getSize();
    }

    /**
     * Copies the warriors instructions to a random but - disjunctive - area in the core.
     */
    public void copyWarriorsToCore() {
        if (this.getInitialWarriorList().size() == 2) {
            // first player
            int startPlayerIndex = identifyStartPlayer();
            int startPlayerFirstInstruction = startPlayerFirstInstructionAtCore();
            initialWarriorList.get(startPlayerIndex).setStartPlayer(true);
            initialWarriorList.get(startPlayerIndex)
                    .setStartAddressAtCore(startPlayerFirstInstruction);

            // local variable
            Warrior startPlayer = initialWarriorList.get(startPlayerIndex);

            boolean startPlayerInstructionPointerSet = false;

            // now iterate over all instructions of the first warrior (start player)
            for (int j = 0; j < startPlayer.getInstructionsCount(); j++) {
                // prepare CoreElement
                InstructionTemplate temp;
                temp = startPlayer.getRcProgram().get(j);
                CoreElement coreElement = convertInstrTemplateToCoreElement(temp);

                // set CoreElement
                playground.setCoreElement(startPlayerFirstInstruction + j, coreElement);

                // find first not-DAT CoreElement and set InstructionPointer to there
                if ((!startPlayerInstructionPointerSet)
                        && coreElement instanceof CoreElementInstruction) {
                    startPlayer.getWarriorProcessQueue().getCurrent()
                            .setInstructionPointer(startPlayerFirstInstruction + j);
                    startPlayerInstructionPointerSet = true;
                }
            }

            translateLabelsOfWarrior(startPlayerFirstInstruction,
                    startPlayer.getInstructionsCount());

            // second player (not allowed to overlap with 1st player!)
            int secondPlayerIndex = (startPlayerIndex == 0) ? 1 : 0;
            initialWarriorList.get(secondPlayerIndex).setStartPlayer(false);
            int secondPlayerFirstInstruction =
                    secondPlayerFirstInstructionAtCore(startPlayerIndex, secondPlayerIndex);
            initialWarriorList.get(secondPlayerIndex)
                    .setStartAddressAtCore(secondPlayerFirstInstruction);

            // local variable
            Warrior secondPlayer = initialWarriorList.get(secondPlayerIndex);

            boolean secondPlayerInstructionPointerSet = false;

            for (int j = 0; j < secondPlayer.getInstructionsCount(); j++) {
                // prepare CoreElement
                InstructionTemplate temp;
                temp = secondPlayer.getRcProgram().get(j);
                CoreElement coreElement = convertInstrTemplateToCoreElement(temp);

                // set CoreElement
                playground.setCoreElement(secondPlayerFirstInstruction + j, coreElement);

                // find first not-DAT CoreElement and set InstructionPointer to there
                if ((!secondPlayerInstructionPointerSet)
                        && coreElement instanceof CoreElementInstruction) {
                    secondPlayer.getWarriorProcessQueue()
                            .getCurrent().setInstructionPointer(secondPlayerFirstInstruction + j);
                    secondPlayerInstructionPointerSet = true;
                }

            }

            translateLabelsOfWarrior(secondPlayerFirstInstruction,
                    secondPlayer.getInstructionsCount());

            // set start player at pos = 0 at list, set second player at pos = 1
            initialWarriorList.set(0, startPlayer);
            initialWarriorList.set(1, secondPlayer);
        } else if (this.getInitialWarriorList().size() == 1) {
            int startPlayerFirstInstruction = startPlayerFirstInstructionAtCore();
            initialWarriorList.get(0).setStartPlayer(true);
            initialWarriorList.get(0).setStartAddressAtCore(startPlayerFirstInstruction);

            // local variable
            Warrior startPlayer = initialWarriorList.get(0);

            boolean startPlayerInstructionPointerSet = false;


            // now iterate over all instructions of the first warrior (start player)
            for (int j = 0; j < startPlayer.getInstructionsCount(); j++) {
                // prepare CoreElement
                InstructionTemplate temp;
                temp = startPlayer.getRcProgram().get(j);
                CoreElement coreElement = convertInstrTemplateToCoreElement(temp);

                // set CoreElement
                playground.setCoreElement(startPlayerFirstInstruction + j, coreElement);

                // find first not-DAT CoreElement and set InstructionPointer to there
                if ((!startPlayerInstructionPointerSet)
                        && coreElement instanceof CoreElementInstruction) {
                    startPlayer.getWarriorProcessQueue().getCurrent()
                            .setInstructionPointer(startPlayerFirstInstruction + j);
                    startPlayerInstructionPointerSet = true;
                }
            }
            translateLabelsOfWarrior(startPlayerFirstInstruction,
                    startPlayer.getInstructionsCount());

        }
    }

    /**
     * Converts instruction templates into CoreElementInstruction / CoreElementDat.
     * @param toConvert the CoreElementInstruction or CoreElementDat to convert
     * @return a CoreElement variable
     * @throws IllegalArgumentException invalid input format
     */
    public CoreElement convertInstrTemplateToCoreElement(InstructionTemplate toConvert)
            throws IllegalArgumentException {
        // TODO error handling (try catch - throw)

        if (toConvert.getOperationCode().equals("DAT")) {
            CoreElementDat result = new CoreElementDat(0,0);
            result.setLabel(toConvert.getLabel());
            result.setHasLabel(toConvert.hasLabel());
            result.setAField(toConvert.getAField());
            result.setBField(toConvert.getBField());
            return result;
        } else {
            CoreElementInstruction result = new CoreElementInstruction();
            result.setLabel(toConvert.getLabel());
            result.setHasLabel(toConvert.hasLabel());
            result.setAField(toConvert.getAField());
            if (toConvert.getBField() != null) {
                result.setBField(toConvert.getBField());
            } else {
                RcArgument temp = new RcArgument();
                temp.setAddress(0);
                temp.setFieldPrefix(FieldPrefix.Direct);
                result.setBField(temp);
            }
            // TODO add try catch here:
            IInstruction temp = getInstructionTypeFromString(toConvert.getOperationCode());
            result.setInstruction(temp);
            return result;
        }
    }

    /**
     * Help function, returns the instruction type that fits to given the string.
     * @param toInstruction string to get instruction type from
     * @return the instruction type that fits to the string
     */
    protected IInstruction getInstructionTypeFromString(String toInstruction) {
        // TODO error handling
        return instructionLocator.getInstructionByOpCode(toInstruction);
    }

    /**
     * Replaces every label usage in a warrior by translating the labels to the corresponding
     * addresses.
     * @param firstInstruction  index of the warrior's first CoreElement in the Core
     * @param instructionsCount length of the warrior
     */
    private void translateLabelsOfWarrior(int firstInstruction, int instructionsCount) {
        AddressingManager addressingManager = new AddressingManager();
        for (int i = firstInstruction; i < (firstInstruction + instructionsCount); i++ ) {
            CoreElement thisCoreElement = playground.getCoreElement(i);
            if (thisCoreElement.getAField().hasReferencedLabel()) {
                int relativeAddress = addressingManager
                        .findLabelInCore(playground, thisCoreElement.getAField());
                thisCoreElement.getAField().setReferencedLabel(null);
                thisCoreElement.getAField().setAddress(relativeAddress);
            }
            if (thisCoreElement.getBField().hasReferencedLabel()) {
                int relativeAddress = addressingManager
                        .findLabelInCore(playground, thisCoreElement.getBField());
                thisCoreElement.getBField().setReferencedLabel(null);
                thisCoreElement.getBField().setAddress(relativeAddress);
            }
        }
        for (int i = firstInstruction; i < firstInstruction + instructionsCount; i++ ) {
            playground.getCoreElement(i).setLabel(null);
            playground.getCoreElement(i).setHasLabel(false);
        }
    }
}
