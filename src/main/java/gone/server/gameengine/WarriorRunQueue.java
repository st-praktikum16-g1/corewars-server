package gone.server.gameengine;

import gone.server.core.Core;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Manages the order of execution for the warrior and their general handling.
 */

public class WarriorRunQueue {
    private java.util.Queue<Warrior> warriorRunQueue;

    /**
     *  Constructor.
     *  Adds the warriors to the queue, the head element is startPlayer.
     *  @param preGeWarriorList list of warriors to be added in the warrior queue
     *  @throws IllegalArgumentException when the warrior list is empty
     */
    public WarriorRunQueue(List<Warrior> preGeWarriorList) throws IllegalArgumentException {
        if (preGeWarriorList.isEmpty()) {
            throw new IllegalArgumentException("WarriorList is empty!");
        }
        warriorRunQueue = new LinkedList<>();
        warriorRunQueue.addAll(preGeWarriorList);
    }

    /* getters */

    /**
     * Returns the next warrior in the warrior queue.
     * @return  next warrior in the warrior queue
     */
    private Warrior getNextWarrior() {
        Warrior temp = warriorRunQueue.remove();
        warriorRunQueue.add(temp);
        return temp;
    }

    public Queue<Warrior> getWarriorRunQueue() {
        return warriorRunQueue;
    }

    public void run(Core core) {
        getNextWarrior().run(core);
    }
}
