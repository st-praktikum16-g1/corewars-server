package gone.server.gameengine;

import gone.server.contracts.IProcess;
import gone.server.contracts.IProcessQueue;
import gone.server.core.Core;

import java.util.LinkedList;

/**
 * Handles the processes of a warrior.
 */

public class WarriorProcessRunQueue implements IProcessQueue {
    private LinkedList<IProcess> processQueue;
    private IProcess current;

    /**
     * Constructor.
     * @param warriorProcess    process to set as first process of this WarriorProcessRunQueue
     */
    protected WarriorProcessRunQueue(IProcess warriorProcess) {
        processQueue = new LinkedList<>();
        processQueue.add(warriorProcess);
        current = processQueue.element();
    }

    /* getters */

    public LinkedList<IProcess> getProcessQueue() {
        return this.processQueue;
    }

    /* class methods */

    /**
     * Returns the next process in this warrior process queue.
     * @return  next process in this warrior process queue
     */
    private IProcess getNextProcess() {
        current = processQueue.remove();
        processQueue.add(current);
        return current;
    }

    @Override
    public void run(Core playground) {
        getNextProcess().run(playground, this);
    }

    @Override
    public void add(IProcess toAdd) {
        processQueue.add(toAdd);
    }

    @Override
    public void remove(IProcess toKill) {
        processQueue.remove(toKill);
    }

    @Override
    public IProcess getCurrent() {
        return current;
    }
}
