package gone.server.gameengine;

import gone.server.contracts.IInstruction;
import gone.server.contracts.IInstructionLocator;
import gone.server.instructions.AddInstruction;
import gone.server.instructions.CompareInstruction;
import gone.server.instructions.DecrementAndJumpIfNotZeroInstruction;
import gone.server.instructions.JumpIfNotZeroInstruction;
import gone.server.instructions.JumpIfZeroInstruction;
import gone.server.instructions.JumpInstruction;
import gone.server.instructions.MoveInstruction;
import gone.server.instructions.SkipIfLessThan;
import gone.server.instructions.SplitInstruction;
import gone.server.instructions.SubInstruction;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains all opcodes of the instructions make the connection between opcode and specific
 * instruction.
 */
public class InstructionLocator implements IInstructionLocator {
    /**
     * Gets a list of valid operation codes for the current Redcode standard.
     * @return list of operation codes as text segments
     */
    @Override
    public List<String> getValidOperationCodes() {
        List<String> result = new LinkedList<>();

        // TODO: dynamically load this from Service Locator
        result.add("DAT");
        result.add("MOV");
        result.add("ADD");
        result.add("SUB");
        result.add("JMP");
        result.add("JMZ");
        result.add("JMN");
        result.add("DJN");
        result.add("CMP");
        result.add("SPL");
        result.add("SLT");

        return result;
    }

    @Override
    public IInstruction getInstructionByOpCode(String opCode) {
        IInstruction result;

        switch (opCode) {
            case "MOV":
                result = new MoveInstruction();
                break;
            case "ADD":
                result = new AddInstruction();
                break;
            case "SUB":
                result = new SubInstruction();
                break;
            case "JMP":
                result = new JumpInstruction();
                break;
            case "JMZ":
                result = new JumpIfZeroInstruction();
                break;
            case "JMN":
                result = new JumpIfNotZeroInstruction();
                break;
            case "DJN":
                result = new DecrementAndJumpIfNotZeroInstruction();
                break;
            case "CMP":
                result = new CompareInstruction();
                break;
            case "SPL":
                result = new SplitInstruction();
                break;
            case "SLT":
                result = new SkipIfLessThan();
                break;
            default:
                throw new IllegalArgumentException("Invalid OperationCode String: " + opCode);
        }
        return result;
    }
}
