package gone.server.gameengine;

/**
 * Information about the current game status, the game loop retrieves if the game is still running
 * and the round number.
 */
public class GameInfo {
    public int roundNumber;
    public boolean isGameRunning;

    /* constructor */

    /**
     * Constructor.
     * TODO why are the parameters given? Aren't they always 0, true, false?
     * @param roundNumber   number of rounds played
     * @param running   true if game is running
     * @param isDraw    true if the game is draw between the players
     */
    public GameInfo(int roundNumber, boolean running, boolean isDraw) {
        setRoundNumber(roundNumber);
        setGameRunning(running);
    }

    /* setters */

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public void setGameRunning(boolean gameRunning) {
        isGameRunning = gameRunning;
    }

    /* getters */

    public int getRoundNumber() {
        return roundNumber;
    }

    public boolean getIsGameRunning() {
        return isGameRunning;
    }
}
