package gone.server.operator;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;

/**
 * Creates a string representation for a given CoreElement.
 */
public final class CoreElementToStringHelper {
    /**
     * Returns the keyword if the given CoreElement is a CoreElementInstructions, for CoreElementDat
     * it returns "CORE_DAT" if the CoreElement's A-field is direkt else "USER_DAT"
     * @param coreElement   the coreElement to return the string representation for
     * @return  String "ADD", "MOV" ... "USER_DAT", "CORE_DAT"
     */
    public static String getStringRepresentationForCoreElement(CoreElement coreElement) {
        if (coreElement instanceof CoreElementInstruction) {
            return ((CoreElementInstruction)coreElement).getInstruction().getKeyword();
        } else {
            CoreElementDat datE = ((CoreElementDat)coreElement);
            if (datE.getAField().getFieldPrefix() == FieldPrefix.Direct) {
                return "CORE_DAT";
            } else {
                return "USER_DAT";
            }
        }
    }
}
