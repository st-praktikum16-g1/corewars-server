package gone.server.operator;

/**
 * Handles the game result if it is not a draw (2 player game).
 */
public class GameResultNoDraw extends GameResult {
    private String winnerWarriorName;
    private String loserWarriorName;

    /* constructor */

    /**
     * Constructor for new GameResultNoDraw.
     * @param isDraw    true if draw
     * @param roundNumber   number of rounds played
     * @param playerCount   number of players in the core
     * @param winnerName    name of the winner of the game
     * @param loserName name of the loser of the game
     * @throws IllegalArgumentException if draw==true, playerCount!=2, winnerName==loserName
     */
    public GameResultNoDraw(boolean isDraw, int roundNumber, int playerCount,
                            String winnerName, String loserName)
            throws IllegalArgumentException {
        super(isDraw, roundNumber, playerCount);
        if (isDraw) {
            throw new IllegalArgumentException("isDraw must be false here!");
        }

        if (this.getPlayerCount() != 2) {
            throw new IllegalArgumentException("winner and loser only existent in 2 player games!");
        }
        if (winnerName.equals(loserName)) {
            // throw new IllegalArgumentException("winner and loser have same name!");
        }
        this.winnerWarriorName = winnerName;
        this.loserWarriorName = loserName;
    }


    /* getter */

    public String getWinnerWarriorName() {
        return winnerWarriorName;
    }

    public String getLoserWarriorName() {
        return loserWarriorName;
    }

}
