package gone.server.operator;

import gone.server.contracts.CoreElement;
import gone.server.contracts.FieldPrefix;
import gone.server.contracts.IGameStepListener;
import gone.server.contracts.IProcess;
import gone.server.core.Core;
import gone.server.core.CoreElementDat;
import gone.server.core.CoreElementInstruction;
import gone.server.gameengine.GameEngine;
import gone.server.gameengine.Warrior;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * executes GameSteps continuously.
 */

public class ServerGameLoop implements Runnable {
    private GameEngine gameEngine;
    private GameResult gameResult;
    private IGameStepListener listener;
    private Timer timer = new Timer();
    private boolean firstPlayerActive;
    private int currentCycle = 0;
    private int timeMs;

    /**
     * Constructor for ServerGameLoop.
     * @param maxCyclesCount limit for how many Cycles can be executed
     * @param preGeWarriorList  warriors that will be put in the warrior run queue
     * @param coreVarFromPge    core that will be set as playground
     * @throws IllegalArgumentException invalid input
     */

    public ServerGameLoop(int maxCyclesCount,
                          List<Warrior> preGeWarriorList,
                          Core coreVarFromPge, int timeMs) throws IllegalArgumentException {
        gameEngine = new GameEngine(maxCyclesCount, preGeWarriorList, coreVarFromPge);
        gameResult = new GameResult(false, 0, GameEngine.getActualPlayersCount());
        this.timeMs = timeMs;
    }

    public void setListener(IGameStepListener gameStepListener) {
        this.listener = gameStepListener;
    }

    /* class methods */

    /**
     * Handles the course of actions of a game, select next Warrior and his process for execution,
     * do the execution, check if game has ended...
     * @throws IllegalStateException invalid input format
     */

    public void gameLoop() throws IllegalStateException {
        currentCycle = 0;

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                innerGameLoop();
            }
        }, timeMs, timeMs);
    }

    /**
     * Getter for the game result.
     * @return  game result so either draw or one has to check which warrior is still alive = winner
     * @throws IllegalStateException when the game is still running
     */
    public GameResult getGameResult() throws IllegalStateException {
        // game is still running
        if (gameEngine.getGameInfo().isGameRunning) {
            throw new IllegalStateException("game is still running");
        } else {
            return gameResult;
        }
    }

    public GameEngine getGameEngine() {
        return gameEngine;
    }

    @Override
    public void run() {
        gameLoop();
    }

    private void innerGameLoop() {
        if (currentCycle < GameEngine.getMaxCyclesCount()) {

            // TODO: idea to get the warrior loser name (performance)
            Warrior warriorToExecute = gameEngine.getWarriorRunQueue().getWarriorRunQueue()
                    .element();
            IProcess currentProcess = warriorToExecute.getWarriorProcessQueue().getProcessQueue()
                    .getFirst();
            int oldIp = currentProcess.getInstructionPointer();

            // execution of 1 game cycle
            gameEngine.gameStep();
            Logger.getGlobal().log(Level.FINEST, "next GameStep");

            int changedIndex = gameEngine.getPlayCore().getChangedCoreElementIndex();
            String newOpCode = null;

            if (changedIndex >= 0) {
                CoreElement ce = gameEngine.getPlayCore().getCoreElement(changedIndex);
                newOpCode = CoreElementToStringHelper.getStringRepresentationForCoreElement(ce);
            }

            int ip = currentProcess.getInstructionPointer();

            if (!warriorToExecute.getWarriorProcessQueue().getProcessQueue()
                    .contains(currentProcess)) {
                ip = -1;
            }

            if (listener != null) {
                listener.gameStep(changedIndex, newOpCode, oldIp, ip, firstPlayerActive);
            }

            if (GameEngine.getActualPlayersCount() == 2) {
                firstPlayerActive = !firstPlayerActive;
            }

            // no draw
            if (!gameEngine.areAllWarriorsAlive() && !gameEngine.getGameInfo().isGameRunning) {
                timer.cancel();
                if (GameEngine.getActualPlayersCount() == 2) {
                    String loserTemp = warriorToExecute.getName();
                    String winnerTemp = "warrior";

                    for (Warrior w:gameEngine.getWarriorRunQueue().getWarriorRunQueue()) {
                        if(warriorToExecute != w) winnerTemp = w.getName();
                    }

                    gameResult = new GameResultNoDraw(false, gameEngine.getGameInfo().roundNumber,
                            2, winnerTemp, loserTemp);
                    if (listener != null) {
                        listener.gameResult(winnerTemp);
                    }

                    return;
                } else {
                    gameResult.setRoundNumber(gameEngine.getGameInfo().roundNumber);
                    return;
                }
            }

            currentCycle++;
        } else if (gameEngine.areAllWarriorsAlive() && !gameEngine.getGameInfo().isGameRunning) {
            // draw
            timer.cancel();
            if (listener != null) {
                listener.gameResult("draw");
            }
            gameResult.setDraw(true);
            gameResult.setRoundNumber(GameEngine.getMaxCyclesCount());

        } else {
            timer.cancel();
            throw new IllegalStateException("undefined game result");
        }
    }
}
