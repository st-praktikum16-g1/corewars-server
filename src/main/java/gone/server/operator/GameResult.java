package gone.server.operator;

import gone.server.contracts.ServerData;

/**
 * Gathers information of the game result, that later can be used.
 * on client-side for visualization etc.
 */
public class GameResult extends ServerData {
    private boolean isDraw;
    private int roundNumber;
    private int playerCount;

    /* constructor */

    /**
     * Constructor.
     * @param isDraw    true if game is draw
     * @param roundNumber   number of played rounds
     * @param playerCount   number of players in the game
     * @throws IllegalArgumentException if roundNumber is <0 or playerCount not 1 or 2
     */
    public GameResult(boolean isDraw, int roundNumber,
                      int playerCount) throws IllegalArgumentException {
        setDraw(isDraw);
        setRoundNumber(roundNumber);
        setPlayerCount(playerCount);
    }

    /* setter */

    /**
     * Sets the number of players in the game.
     * @param value amount of players in the game
     * @throws IllegalArgumentException if the given value is not 1 or 2
     */
    private void setPlayerCount(int value) throws IllegalArgumentException {
        if (value != 1 && value != 2) {
            throw new IllegalArgumentException("player count must be 1 or 2");
        }
        this.playerCount = value;
    }

    public void setDraw(boolean value) {
        isDraw = value;
    }

    /**
     * Setter for the round played so far.
     * @param value number of rounds played so far
     * @throws IllegalArgumentException if the given value is <0
     */
    public void setRoundNumber(int value) throws IllegalArgumentException {
        if (value < 0) {
            throw new IllegalArgumentException("round number must be >= 0");
        }
        this.roundNumber = value;
    }

    /* getter */

    public int getPlayerCount() {
        return playerCount;
    }

    public boolean isDraw() {
        return isDraw;
    }

    public int getRoundNumber() {
        return roundNumber;
    }
}
