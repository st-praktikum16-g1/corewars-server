package gone.server.operator;

import gone.server.contracts.IGameEngine;
import gone.server.contracts.IOperator;
import gone.server.contracts.IPlayerQueue;
import gone.server.contracts.IPreGameEngine;
import gone.server.contracts.IRedcodeParser;

/**
 * Not used.
 */
public class Operator implements IOperator {
    private IRedcodeParser rcParser;
    private IPreGameEngine preGameE;
    private IGameEngine gameEngine;
    private IPlayerQueue playerQueue;

    // TODO: implement, add Interface ICommManager
}
