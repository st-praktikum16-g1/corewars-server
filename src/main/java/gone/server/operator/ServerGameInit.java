package gone.server.operator;

import gone.server.core.Core;
import gone.server.gameengine.PreGameEngine;
import gone.server.gameengine.Warrior;
import gone.server.gameengine.WarriorProcess;
import gone.server.parser.RedcodeParser;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Uses parser and preGameEngine to initiate the game on server side.
 */

public class ServerGameInit {
    private RedcodeParser rcParser;
    private PreGameEngine preGameE;


    /**
     * Constructor for 1 player.
     * @param redCodePlayer redcode for the warrior of the player
     * @param namePlayer    name for the player
     * @param playCoreSize  size of the core to create
     * @throws IllegalArgumentException if parsing of the warrior fails or problems in the PGE
     */
    public ServerGameInit(String redCodePlayer, String namePlayer, int playCoreSize)
            throws IllegalArgumentException {
        rcParser = new RedcodeParser();
        Warrior player1 = new Warrior(namePlayer, new WarriorProcess(),
                rcParser.parse(redCodePlayer));
        preGameE = new PreGameEngine(playCoreSize, 1, new Core(playCoreSize));
        preGameE.setInitialWarriorList(player1);
        preGameE.copyWarriorsToCore();
    }

    /**
     * Constructor for 2 players.
     * @param redCodePlayer1    redcode warrior for player 1
     * @param namePlayer1   name of player 1
     * @param redCodePlayer2    redcode warrior for player 2
     * @param namePlayer2   name of player 1
     * @param playCoreSize  size of the core to create
     * @throws IllegalArgumentException if parsing of a warrior fails or problems in the PGE
     */
    public ServerGameInit(String redCodePlayer1, String namePlayer1,
                          String redCodePlayer2, String namePlayer2, int playCoreSize)
            throws IllegalArgumentException {
        Logger.getGlobal().log(Level.INFO, "starting ServerGameInit with two players...");
        rcParser = new RedcodeParser();
        Warrior player1 = new Warrior(namePlayer1, new WarriorProcess(),
                rcParser.parse(redCodePlayer1));
        Logger.getGlobal().log(Level.FINE, "Created Warrior for Player 1");
        Warrior player2 = new Warrior(namePlayer2, new WarriorProcess(),
                rcParser.parse(redCodePlayer2));
        Logger.getGlobal().log(Level.FINE, "Created Warrior for Player 2");

        preGameE = new PreGameEngine(playCoreSize / 2, 2, new Core(playCoreSize));
        Logger.getGlobal().log(Level.FINE, "Created PreGameEngine");

        preGameE.setInitialWarriorList(player1, player2);
        preGameE.copyWarriorsToCore();
        Logger.getGlobal().log(Level.FINE, "ServerGameInit done");

    }

    /* getter */

    public Core getInitiatedCore() {
        return preGameE.getPlayground();
    }

    public PreGameEngine getPreGameE() {
        return this.preGameE;
    }
}
