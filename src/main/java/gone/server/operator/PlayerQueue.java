package gone.server.operator;

import gone.server.contracts.IPlayerQueue;
import gone.server.contracts.Player;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;

/**
 * Queue that contains the players and ad information about them.
 */

public class PlayerQueue implements IPlayerQueue {
    private List<Player> queue;
    private boolean matchReady;

    /* constructor */

    public PlayerQueue(List<Player> queue, boolean matchReady) {
        setQueue(queue);
        setMatchReady(matchReady);
    }

    public PlayerQueue() {
        queue = new LinkedList<>();
        matchReady = false;
    }

    /* setters */

    public void setQueue(List<Player> value) {
        // TODO value check
        this.queue = value;
    }

    private void setMatchReady(boolean value) {
        // TODO value check
        this.matchReady = value;
    }

    public void addPlayer(Player input) {
        // TODO: error check (parameters of variable input)
        queue.add(input);
    }

    /* getters */

    /**
     * Returns a list of players in the PlayerQueue.
     * @return  a list of all players in this playerQueue
     */
    public List<Player> getReadyPlayers() {
        List<Player> result = new LinkedList<>();

        for (Player player : this.queue) {
            if (player.isReady()) {
                result.add(player);
            }
        }

        return result;
    }

    public List<Player> getQueue() {
        return queue;
    }

    public boolean isMatchReady() {
        return matchReady;
    }

    /* class methods */

    public void refresh(){
        // TODO: implement
    }

    /**
     * Returns the player with the given sessionId.
     * @param sessionId of the player wie want
     * @return  Player with the given sessionId
     * @throws IllegalArgumentException if player couldn't be found
     */
    public Player getPlayerBySessionId(String sessionId) throws IllegalArgumentException {
        for (Player player : queue) {
            if (player.getSessionId().equals(sessionId)) {
                return player;
            }
        }
        throw new IllegalArgumentException("Player with sessionId: " + sessionId
        + "not found!");
    }
}
