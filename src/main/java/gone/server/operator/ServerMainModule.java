package gone.server.operator;

import gone.lib.common.ClientRole;
import gone.lib.common.RcStandard;
import gone.lib.config.ConfigTypes;
import gone.lib.config.LoadConfigFile;
import gone.lib.config.SaveConfigFile;
import gone.lib.config.ServerConfig;
import gone.lib.network.CwServer;
import gone.lib.network.contracts.IServer;
import gone.lib.network.contracts.IServerCallback;
import gone.lib.network.json.*;
import gone.server.contracts.CoreElement;
import gone.server.contracts.IGameStepListener;
import gone.server.contracts.Player;
import gone.server.core.Core;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.InvalidPreferencesFormatException;

import javax.naming.OperationNotSupportedException;

/**
 * Main module of the Server, controls everything.
 */
public class ServerMainModule implements IServerCallback, IGameStepListener {
    private PlayerQueue playerQueue = new PlayerQueue();
    private List<String> spectatorQueue = new LinkedList<String>();
    private IServer server;

    private Player playerOne;
    private Player playerTwo;

    private ServerGameLoop gameLoop;

    private ServerConfig serverConfig;
    private int coreSize;
    private int port;
    private int maxCycles;
    private int maxStartInstructions;
    private int waitTimeMs;

    private boolean gameRunning;
    private Core core;

    /**
     * Constructor for the ServerMainModule.
     * @throws IOException  when problems with loading/saving the config file
     * @throws InvalidPreferencesFormatException if the config file's XML is not valid
     */
    public ServerMainModule() throws IOException, IllegalAccessException, InvalidPreferencesFormatException {
        String serverPath = System.getProperty("user.home") + File.separator + "gone.corewars"
                + File.separator + "config" + File.separator + "server" + File.separator
                + "defaultServerConfig.xml";

        if (!new File(serverPath).exists()) {
            serverConfig = new ServerConfig(10000, 1000, 5556, 2, 3000, 200);
            new SaveConfigFile(serverConfig,serverPath, ConfigTypes.SERVER_CONFIG);
            Logger.getGlobal().log(Level.INFO,
                    "created new server config file with standard values");
        } else {
            LoadConfigFile load = new LoadConfigFile();
            Logger.getGlobal().log(Level.INFO, "loaded server config file");

            load.loadServerPrefs(serverPath, ConfigTypes.SERVER_CONFIG);
            serverConfig = (ServerConfig) load.getConfigParameters();
        }

        coreSize = serverConfig.getCoreSize();
        port = serverConfig.getPort();
        maxCycles = serverConfig.getMaxCycles();
        maxStartInstructions = serverConfig.getMaxStartInstructions();
        waitTimeMs = serverConfig.getWaitTimeMs();

        String configString = "CoreSize: " + coreSize
                + "\nPort: " + port
                + "\nMaxCycles: " + maxCycles
                + "\nMaxStartInstructions: " +  maxStartInstructions
                + "\nWaitTimeMs: " + waitTimeMs
                + "\nMaxPlayerCount: " + serverConfig.getMaxPlayersCount();
        Logger.getGlobal().log(Level.INFO, configString);

        server = new CwServer(this);
        try {
            server.start(port);
        } catch (OperationNotSupportedException exception) {
            exception.printStackTrace();
        }

        Logger.getGlobal().log(Level.INFO, "Server started");
    }

    @Override
    public void receiveLoginTelegram(CwLoginContent cwLoginContent, String sessionId) {
        if (cwLoginContent.role == ClientRole.Viewer) {
            spectatorQueue.add(sessionId);
        } else {
            // put client to queue
            Player player = new Player(cwLoginContent.name, sessionId, true, 0, 0, false);
            playerQueue.addPlayer(player);

            CwLoginOkContent content = new CwLoginOkContent();
            content.standard = RcStandard.ICWS88;
            content.clientMustWait = false;
            content.coreSize = coreSize;
            content.lineLength = 999;

            Logger.getGlobal().log(Level.INFO, "login received: " + cwLoginContent.name);

            server.sendLoginOkTelegram(content, sessionId);

            Logger.getGlobal().log(Level.INFO, "login ok sent");
        }
    }

    @Override
    public void receiveReadyTelegram(CwReadyContent cwReadyContent, String sessionId) {
        if(spectatorQueue.contains(sessionId)) {
            if(gameRunning) {
                CwStartContent startTelegram = getStartTelegram();
                server.sendStartTelegram(startTelegram, sessionId);
            }

            return;
        }

        // check if correct client
        // start game if needed
        // serializer.serializeGameStatusMessage() started
        playerQueue.getPlayerBySessionId(sessionId).setReady(true);

        if (cwReadyContent.warriorRedcode.size() > maxStartInstructions) {
            Logger.getGlobal().log(Level.WARNING, "Player rejected (Redcode to long)");
            return;
        }

        playerQueue.getPlayerBySessionId(sessionId).setRedcode(String.join("\n",
                cwReadyContent.warriorRedcode));
        List<Player> readyPlayers = playerQueue.getReadyPlayers();

        Logger.getGlobal().log(Level.INFO, "ready received: "
                + cwReadyContent.warriorRedcode.size() + " (lines)\n");

        if (readyPlayers.size() >= 2 && !gameRunning) {
            gameRunning = true;
            // start game
            playerOne = readyPlayers.get(0);
            playerTwo = readyPlayers.get(1);

            if(playerOne.getName().equals(playerTwo.getName())) {
                playerOne.setName(playerOne.getName() + " 1");
                playerTwo.setName(playerTwo.getName() + " 2");
            }

            ServerGameInit init = new ServerGameInit(playerOne.getRedcode(), playerOne.getName(),
                    playerTwo.getRedcode(), playerTwo.getName(), coreSize);

            core = init.getInitiatedCore();

            CwStartContent startTelegram = getStartTelegram();

            server.sendStartTelegram(startTelegram, playerOne.getSessionId());
            server.sendStartTelegram(startTelegram, playerTwo.getSessionId());

            for (String spectator : new LinkedList<>(spectatorQueue)) {
                try {
                    server.sendStartTelegram(startTelegram, spectator);
                } catch (Exception exception) {
                    spectatorQueue.remove(spectator);
                    // spectator is dead
                }
            }

            gameLoop = new ServerGameLoop(maxCycles, init.getPreGameE()
                    .getInitialWarriorList(), init.getInitiatedCore(), waitTimeMs);
            Logger.getGlobal().log(Level.INFO, "new ServerGameLoop created");
            gameLoop.setListener(this);
            new Thread(gameLoop).start();

            Logger.getGlobal().log(Level.INFO, "game started");
        }
    }

    private CwStartContent getStartTelegram() {
        CwStartContent startTelegram = new CwStartContent();
        startTelegram.fullState = new LinkedList<>();

        for (CoreElement ce : core.getCoreList()) {
            startTelegram.fullState
                    .add(CoreElementToStringHelper.getStringRepresentationForCoreElement(ce));
        }

        startTelegram.players = 2;
        return startTelegram;
    }

    @Override
    public void gameStep(int changedOpCodeIndex, String newOpCode, int oldIp, int newIp,
                         boolean firstPlayerActive) {
        CwGameStatusContent gameStatus = new CwGameStatusContent();

        gameStatus.newOpCode = newOpCode;
        gameStatus.indexOfChangedCoreElement = changedOpCodeIndex;
        gameStatus.newIpIndex = newIp;
        gameStatus.oldIpIndex = oldIp;
        gameStatus.playerOneActive = firstPlayerActive;

        Logger.getGlobal().log(Level.INFO, "game step: opCode - " + newOpCode + " firstPlayer - "
                + firstPlayerActive);

        // send game data to both clients
        server.sendGameStatusTelegram(gameStatus, playerOne.getSessionId());
        server.sendGameStatusTelegram(gameStatus, playerTwo.getSessionId());

        for (String spectator : new LinkedList<>(spectatorQueue)) {
            try {
                server.sendGameStatusTelegram(gameStatus, spectator);
            } catch (Exception exception) {
                spectatorQueue.remove(spectator);
                // spectator is dead
            }
        }

        Logger.getGlobal().log(Level.INFO, "game status sent");
    }

    @Override
    public void gameResult(String playerName) {
        if(!gameRunning) return;
        gameRunning = false;

        this.playerQueue.getQueue().remove(playerOne);
        this.playerQueue.getQueue().remove(playerTwo);

        CwGameResultContent gameResult = new CwGameResultContent();
        gameResult.state = playerName;

        Logger.getGlobal().log(Level.INFO, "game result: " + playerName);

        // send game data to both clients
        server.sendGameResultTelegram(gameResult, playerOne.getSessionId());
        server.sendGameResultTelegram(gameResult, playerTwo.getSessionId());

        for (String spectator : new LinkedList<>(spectatorQueue)) {
            try {
                server.sendGameResultTelegram(gameResult, spectator);
            } catch (Exception exception) {
                spectatorQueue.remove(spectator);
                // spectator is dead
            }
        }

        spectatorQueue.clear();
        Logger.getGlobal().log(Level.INFO, "game result sent");
    }

    public void teardown() {
        this.server.stop();
    }
}